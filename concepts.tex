\section{\Name: Design and Implementation} \label{sec:concepts}

This section explains the main decisions in the design and implementation of \Name, and the flow of its process. 

\subsection{Main Concepts}

In order to tackle the issues faced in the reproduction study, three
concepts are introduced. The first is \textit{Feature Engineering} that
forms the features in a way that they are proper input values to
train and prediction processes. This step, explained in Section
\ref{sec:feature-eng}, addresses the first issue explained in Section
\ref{sec:feas_lessons}. In order to tackle two problems of candidate
explosion, and lack of semantic features, we introduce a novel filter
named \textit{Action Filter} that captures the similarity of two
methods in terms of the fine-grained actions they carry out to reach
their final goal. This filter is elaborated in Section
\ref{sec:action-filter}. Another concept is \textit{Input
  Partitioning} with the aim of further reducing the number of
candidates and mitigating candidate explosion problem; this concept is
described in Section \ref{sec:indexpar}.

\subsubsection{Feature Engineering} \label{sec:feature-eng}
%Features play a pivotal role in our approach as they are used in training models and in predicting the clone pairs; hence, Feature Engineering is an important step. 
We did Feature Engineering in two parts, as follows. First, we conducted feature selection, and then we scaled the features so that they convey useful information and do not confuse the model. 

\textbf{Feature Selection}. For many algorithms in machine learning it is advised to remove the features which are highly correlated~\cite{hall1999correlation}. Random-Forrest however, doesn't get affected much by correlated features. %The decision-trees used in Random-forest, during training, calculate the information gain for each feature at every step and then learn to make a decision. The highly correlated features, at a given step, will have a very similar information gain and therefore only one of them will be used to make a decision on that step. This makes having correlated features redundant yet not affecting the model negatively. 
As a result, we did not have to analyze the correlations among features heavily. However, we found some interesting patterns in features that needed attention: We noticed that NLOC and NOS values for a very similar code could be different. This is because NLOC captures the number of lines of code, whereas NOS captures the number of statements in code. For example, one could write three statements in one line, decreasing the NLOC value without affecting the number of statements, which will still be three. Thus, NLOC would not have useful information gain for our model; hence, we removed NLOC and reserved NOS. We also removed MOD, which captures the number of modifiers in a method. It is a binary feature (0 or 1) and we noticed that it has no correlation with \textit{is\_clone} label. There is still more scope for feature selection but we decided to go ahead with the remaining features. In future we would do a thorough analyses to reduce as many features as possible. 

\textbf{Feature Scaling}. We noticed in the reproduction study that Percentage Difference between the metric values is a good feature but there were many false negatives in our predictions. Analyses showed that percentage difference could mislead our model while training; let's see this with the help of an example. Consider a candidate pair $M_1,M_2$ where $M_1$ has $n_1$ loops and $M_2$ has $n_2$ number of loops such that $n_1>n_2$. The percentage difference between their number of loops, given by~\ref{perdiff}, will be.
\begin{equation}
\dfrac{n_1-n_2}{n_1}*100
\end{equation}
The percentage difference does not scale well when we increase $n_1$ and $n_2$. If $n_1$ is 2 and $n_1$ is 1, the percentage difference is 50. However, if $n_1$ is 10 and $n_2$ is 9 the percentage difference is 10. Notice in both cases the absolute difference is 1. Also, if $n_2$ is 0, then for any natural number value of $n_1$, the percentage difference will be 100\%; so there is a definite need of feature scaling. We addressed this by using a very simple technique: We added a constant c to both $n_1$ and $n_2$, where c is a natural number. For most of our metrics which needed scaling, their metric values were in range of 0 to 15. Hence, a very big value of c would have removed important information from the features and a small value like 1 would not scale the features. Considering this and assessing different numbers, we decided to set c to 11. The result of this was to use the formula shown in~\ref{newperdiff} for Feature Engineering.

\begin{equation}\label{newperdiff}
Percentage-diff(f1,f2)=\dfrac{|f1-f2|}{Max(f1,f2)+11}*100
\end{equation}

\subsubsection{Action Filter} \label{sec:action-filter}

The software metrics used here mostly capture structural properties of code and do not capture any semantics. Hence, including a semantic feature would assure us that detected cloned methods are similar not only in terms of structural properties, but also in semantics. After exploring possible features, we could not find a meaningful semantical feature that has enough information gain for machine learning process. Thus, we decided to capture the semantic similarity one step before preparing feature vectors, and filter out pairs that are not semantically similar before feeding them to the main process. This filter, named \textit{Action Filter}, captures the fine grained actions a method does to attain its final result. The logic behind this filter is that if two cloned methods are doing the same thing, they may call the same class library methods and reference same class attributes. This is because when one copies a method, and changes it to fit her needs, she will not change the library calls and attributes referenced from class libraries because they are providing a functionality that does not need to be changed or re-written. An example of this notion is illustrated in the following code example:

\begin{lstlisting} [caption=Action Filter Example] 
public static String getEncryptedPassword(String password) throws InfoException {
	StringBuffer buffer = new StringBuffer();
	try {
		byte[] encrypt = password.getBytes("UTF-8");
		MessageDigest md = MessageDigest.getInstance("SHA");
		md.update(encrypt);
		byte[] hashedPasswd = md.digest();
		for (int i = 0; i < hashedPasswd.length; i++) {
			buffer.append(Byte.toString(hashedPasswd[i]));
		}
	} catch (Exception e) {
		throw new InfoException(LanguageTraslator.traslate("474"), e);
	}
	return buffer.toString();
}
\end{lstlisting}

In the example above, which aims to convert its input argument to an encrypted format, Action Tokens are \textit{getBytes()}, \textit{getInstance()}, \textit{update()}, \textit{digest()}, \textit{length}, \textit{append()}, \textit{toString()}, and \textit{translate()}. If one wants to clone this method to get an encrypted string, she most probably will not change many of its Action Tokens. Hence, we can utilize these tokens to capture the semantic similarity between methods. Moreover, the confidence of rejecting a pair increases as the similarity between the Action Tokens of this pair decreases. This allows us to set a threshold parameter on their similarity. 

We use overlap-similarity to calculate the similarity between the Action Tokens of two methods. Equation~\ref{overlap} shows the function to calculate the overlap similarity, where $A_1$ and $A_2$ are sets of Action Tokens in methods $M_1$ and $M_2$ respectively. Each element in these sets is defined as $<t,freq>$, where $t$ is the Action Token and $freq$ is the number of times this token appears in the method.

\begin{equation}\label{overlap}
Sim(A_1,A_2)= |A_1\cap A_2|
\end{equation}

This filter helps us produce a small index by generating fewer but semantically similar candidates. In fact, the strength of this filter lies in the fact that while eliminating many candidates, it maintains a high recall because it captures important semantical information from code. Also by reducing the number of candidates, Action Filter contributes to making the proposed approach both fast and scalable. Moreover, the index creation needs no global information about the dataset, preserving the incremental nature of our approach.

\subsubsection{Two level Input Partitioning} \label{sec:indexpar}
As described before, applying a supervised learning technique to clone detection problem needs pairing each possible method with another in terms of a feature vector. This produces a huge number of candidates while many of them have a drastic difference and do not give useful information to the classification model. In order to further reduce the number of candidates, we did an Input Partitioning based on methods sizes. The idea is that two methods which are very different in their sizes are very likely implementing different functionalities and so they should not be clones. We define method size as the number of tokens each method has, where tokens are language keywords, literals (strings literals are split on whitespace), and identifiers. We borrowed the definition of tokens from~\cite{sajnani2016sourcerercc}.

If the defined similarity threshold is \textit{T\%}, and method $M_1$ has $x$ number of tokens, then, if a method $M_2$ is a clone of $M_1$, its number of tokens should be in the range provided in \ref{eq:indexpar}.
\begin{equation}\label{eq:indexpar}
[x*T\%,\dfrac{x}{T\%}]
\end{equation}

Thus, we made partitions of possible query and candidates based on their sizes, and only did the method pairing locally inside each partition. As the methods are not uniformly distributed with their size, the size-based partitioning creates different-sized partitions. This means some partitions are bigger than others. To make sure that we can load these partitions in memory we further partition the bigger partitions horizontally into smaller sub-partitions such that they could be easily loaded into the memory. These smaller partitions are then indexed in memory one at a time. The queries in a partition is then executed on all of its sub-partitions. This way we do not have to query the entire dataset, but only a smaller subset of it, which reduces the number of candidates while maintaining the recall. The idea of input partitioning is not new, and has been used in information-retrieval many times ~\cite{livieri:2007icse,cambazoglu2006effect,kulkarni2010document}. Researchers in other fields have explored partitioning based on size and also horizontal partitioning to solve the scalability and speed issues~\cite{liebeherr1993effect}. Moreover, Svajlenko and Roy used horizontal partitioning to do in memory clone-detection and achieved very high speed~\cite{svajlenko2017fast}. As per our knowledge we are first to propose and use two-level partitions of indexes for clone detection.


