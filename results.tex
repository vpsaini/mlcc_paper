\section{Model Selection}
Having the dataset consisting of method pairs, their relationships on terms of JHawk features, and  \textit{is\_clone} tags ready, a clone detection model should be trained that can learn the characteristics of clone and non-clone method pairs with the goal of predicting \textit{is\_clone} labels on unseen method pairs. The first step to train the target clone detector model is to select a classification algorithm which fits our needs and has a satisfactory performance both in terms of accuracy and timing. To this aim, we decided to perform N-fold cross validation on the train dataset using various algorithms and measure each algorithm's performance. However, since our training dataset was 45GB in size, training multiple models using N-fold cross validation on the whole train dataset was not feasible in terms of timing. Hence, we selected three different random samples with different sizes from this dataset : a sample with 10 thousand rows, a sample with 50 thousand rows, and another with 100 thousand rows. Having three different samples could help us get assured that we are not getting biased toward a specific sample of data and our model decision process is general and independent of characteristics of specific datasets. The process of N-fold cross validation and its results will be discussed in the following subsections.
		\label{sec:nfoldCV}
		\subsection{N-Fold Cross Validation}
		N-fold cross validation is a process to validate various learning algorithms and make a selection on how well each model will perform \ref{refaeilzadeh2009cross}. 10-fold cross validation is a common form of N-fold cross validation that we used too. For the purpose of 10-fold cross validation, first we divided each of the three sampled datasets to two parts: a 70\% portion named training, and a 30\% named testing. 10-fold cross validation was performed on the 70\% training dataset, and then each model was trained on the whole 70\% training dataset and tested on 30\% testing dataset. The purpose was to reserve an unseen portion in each sample and compare the results of 10-fold cross validation with the results on an unseen bunch of data as well. This could assure us more as to the generalization of our results. We ran our experiments on multiple classifiers: K-Nearest Neighbors (KNN) ~\cite{wu2008top}, Naive Bayes ~\cite{wu2008top} , CLassification and Regression Trees (CART) ~\cite{wu2008top}, Support Vector Machine (SVM) ~\cite{wu2008top}, Logistic Regression (LR) ~\cite{dreiseitl2002logistic}, Linear Discriminant Analysis (LDA) ~\cite{izenman2008modern}.  Performance of each examined classification model in 10-fold cross validation on three sampled datasets is given in Table \ref{10CV}, and  performance of each classifier after being trained on 70\% training dataset and validating on the 30\% unseen testing data is given in Table \ref{test7030}. All percentages in these tables are rounded to the nearest integers, and time measurements, which are in seconds, when possible are rounded to one decimal point. 
		
		\begin{table*}[t]
			\centering
			\caption{Results of 10-fold Cross Validation on Sampled Rows from Train Dataset}
			\label{10CV}
			\begin{center}
			\begin{tabular}{|l|c|c|c|c|c|c|c|c|c|c|c|c|c}
				\cline{1-13}
				Dataset       & \multicolumn{4}{l|}{10 Thousand Rows} & \multicolumn{4}{l|}{50 Thousand Rows} & \multicolumn{4}{l|}{100 Thousand Rows} &  \\ \cline{1-13}
				\backslashbox {Model}{Result} & Prec.   & Recall  & F1    & Time  & Prec.   & Recall  & F1    & Time  & Prec.   & Recall  & F1    & Time   &  \\ \cline{1-13}
				KNN (K=5)     & 92\%        & 92\%    & 92\%  & 1.6   & 92\%        & 96\%    & 94\%  & 33.6  & 93\%        & 97\%    & 95\%  & 145.3  &  \\ \cline{1-13}
				KNN (K=10)    & 91\%        & 94\%    & 93\%  & 1.5   & 92\%        & 97\%    & 94\%  & 30.3  & 93.4\%      & 97\%    & 95\%  & 135.3  &  \\ \cline{1-13}
				Naive Bayes   & 89\%        & 93\%    & 90\%  & 0.2   & 90\%        & 92\%    & 91\%  & 1.5   & 90\%        & 92\%    & 91\%  & 2.6 s  &  \\ \cline{1-13}
				CART          & 89\%        & 95\%    & 91\%  & 0.6   & 92\%        & 96\%    & 94\%  & 3.2   & 93\%        & 97\%    & 95\%  & 7.2 s  &  \\ \cline{1-13}
				SVM           & 96\%        & 89\%    & 93\%  & 31.7  & 96\%        & 93\%    & 94\%  & 1632  &             &         &       &        &  \\ \cline{1-13}
				LR            & 92\%        & 94\%    & 93\%  & 1.1   & 91\%        & 93\%    & 92\%  & 7.4   & 92\%        & 93\%    & 92\%  & 18.1   &  \\ \cline{1-13}
				LDA           & 87\%        & 94\%    & 90\%  & 0.3   & 88\%        & 94\%    & 91\%  & 1.9   & 88\%        & 94\%    & 91\%  & 4.6    &  \\ \cline{1-13}
				Random Forest & 93\%        & 94\%    & 94\%  & 0.5   & 94\%        & 96\%    & 95\%  & 2.7   & 95\%        & 97\%    & 96\%  & 5.7    &  \\ \cline{1-13}
				AdaBoost     & 93\%        & 934\%   & 94\%  & 2.9   & 94\%        & 96\%    & 95\%  & 11.7  & 95\%        & 97\%    & 96\%  & 24.4   &  \\ \cline{1-13}
			\end{tabular}
	%	\end{table}
			
	%\begin{table}[]
		%\centering
		\caption{Results of Training Models and Validating Them on Unseen Portion of Sampled Rows from Train Dataset}
		\label{test7030}
		\begin{tabular}{|l|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|}
			\hline
			Dataset       & \multicolumn{5}{l|}{10 Thousand Rows}                 & \multicolumn{4}{l|}{50 Thousand Rows} &              & \multicolumn{5}{l|}{100 Thousand Rows}               \\ \hline
			\backslashbox {Model}{Result} & Prec. & Recall & F1   & T\textsubscript{L} & T\textsubscript{P} & Prec. & Recall & F1   & T\textsubscript{L} & T\textsubscript{P} & Prec. & Recall & F1   & T\textsubscript{L} & T\textsubscript{P} \\ \hline
			KNN (K=5)     & 90\%      & 92\%   & 91\% & 0.03       & 0.7          & 93\%      & 97\%   & 95\% & 0.2       & 21           & 94\%      & 97\%   & 95\% & 0.7       & 68.7         \\ \hline
			KNN (K=10)    & 91\%      & 94\%   & 93\% & 0.02       & 0.7          & 93\%      & 98\%   & 95\% & 0.2       & 17           & 94\%      & 97\%   & 96\% & 0.8       & 58.4         \\ \hline
			Naive Bayes   & 86\%      & 92\%   & 89\% & 0.02       & 0.01         & 92\%      & 92\%   & 92\% & 0.1       & 0.05         & 90\%      & 91\%   & 91\% & 0.2       & 0.08         \\ \hline
			CART          & 88\%      & 95\%   & 91\% & 0.1        & 0.008        & 92\%      & 97\%   & 94\% & 0.3       & 0.03         & 93\%      & 96\%   & 95\% & 0.7       & 0.05         \\ \hline
			SVM           & 93\%      & 89\%   & 91\% & 3.6        & 1.4          & 97\%      & 93\%   & 95\% & 211       & 35.4         & 96\%      & 94\%   & 95\% & 2079      & 143          \\ \hline
			LR            & 89\%      & 93\%   & 91\% & 0.1        & 0.005        & 92\%      & 93\%   & 93\% & 0.7       & 0.03         & 91\%      & 92\%   & 92\% & 1.6       & 0.05         \\ \hline
			LDA           & 87\%      & 94\%   & 90\% & 0.03       & 0.01         & 89\%      & 94\%   & 92\% & 0.2       & 0.04         & 87\%      & 93\%   & 90\% & 0.3       & 0.06         \\ \hline
			Random Forest & 91\%      & 93\%   & 92\% & 0.04       & 0.01         & 94\%      & 96\%   & 95\% & 0.2       & 0.03         & 95\%      & 96\%   & 95\% & 0.5       & 0.09         \\ \hline
			AdaBoost     & 89\%      & 94\%   & 91\% & 0.2        & 0.01         & 95\%      & 96\%   & 95\% & 0.9       & 0.04         & 94\%      & 96\%   & 95\% & 2.9       & 0.09         \\ \hline
		\end{tabular}
	\end{center}\mbox{}
	\end{table*}
	%\linebreak
	In fact, at the first step of model evaluation, we did not include Random Forest and AdaBoost in our experiments. We tried simple classifiers, and noted down their performance. Keeping this in mind, as the results in tables and shows, KNN (with both values for K), CART and SVM, had a better accuracy than Naive Bayes, LR , and LDA on our data.  We selected these algorithms at the first step of model selection and then proceeded further with the aim of selecting those models that take less time, especially in prediction step. In this step, we excluded KNN and SVM since as the results show, these two are much more slower than CART. The result of this step was selecting CART as our target classifier.  However, classification models are prone to get biased and overfitted to train data and not to generalize to unseen data well. As a result, we decided to test an ensemble classifiers based on our selected classification algorithm (CART) to prevent this risk. Using algorithms that work based on multiple classifiers rather than a single one would prevent overfitting and getting biased toward the characteristics on train dataset, and can contribute to making our model as general as possible. To this end, we selected AdaBoost ~\cite{wu2008top} and Random Forest ~\cite{ho1995random}; both of these algorithms build an ensemble of classifiers: Random Forest builds several Decision Trees from the train dataset and takes the majority vote as the final vote, and AdaBoost builds several models of a configured base model, and combines them ~\cite{wu2008top}. In these experiments,the number of estimators in both Random Forest and Adaboost algorithms were 5, and as mentioned, the base classifier in Adaboost was CART. The reason behind selecting  5 estimators is that in this step our aim was just to understand how an ensemble of CART classifiers would work in comparison with one single CART classifier; the decisions regarding configuring two models including the number of classifiers and CART pre-prunning parameters was postponed to the next step. Having selected CART as the primary classifier, we proceeded to training Random Forest and AdaBoost. As it was expected, accuracy got improved when multiple CART classifiers were working together; however, Random Forest and AdaBoost took more time than CART to perform which was anticipated; but the increase was trivial. As a result of all these observations, we shortlisted Random Forest, and AdaBoost (with CART as the base algorithm) to proceed to the next step. The next steps included evaluating different configurations of these classifiers to select the optimum configuration, and also measuring their performance, especially with respect to timing on a larger dataset. \linebreak
	As mentioned, the goal of  next step was to select one of the two ensemble methods with an optimal configuration. To this end, we decided to run experiments on our sampled datasets and then proceed with a select set of configurations to the whole train dataset. After doing 10-fold cross validation, and training on 70\% of data and testing on 30\% of it, using multiple parameters on the sampled datasets, we found 11 configurations as the ones who were performing better than others. Results for 11 shortlisted configurations on the 100 thousand rows dataset are given in Table \ref{shortlist100k}. \linebreak
	Having shortlisted 11 classifiers, we wanted to choose one based on its performance on the whole training dataset using 10-fold and 70\%-30\% training-testing. However, the whole training dataset had 173,766,592 rows, and since our selected algorithms needed all their data in memory while training their models, performing 10-fold cross validation on all of these rows were not feasible. After analyzing the training dataset, we observed that there are 22,151,199 clone pairs, and 151,615,393 non-clone pairs present in the train dataset. These numbers showed that there are approximately seven times more non-clone pairs than clone pairs present in our dataset. We decided to have a sampling over the train dataset which is both inclusive and manageable. We did a sampling in which beside including all clone pairs, 22,151,199 random non-clone pairs were selected; this yielded to a dataset that included equal number of clones and non-clones. As a result, the new sampled train dataset, which we call large-scale sample, had 44,302,398 rows which was manageable for performing cross validation. Performance of each shortlisted classifier on the large-scale sample in given in . \linebreak
	As Table shows, %##
	configuration had the best performance compared to other configuration. Thus, we selected this configuration as our final classifier, and trained a model with this configuration on the large-scale sample. Performance of this model on unseen test datasets are presented in Section \ref{sec:eval}.

% Please add the following required packages to your document preamble:
% \usepackage{multirow}
\begin{table}[]
	\centering
	\caption{My caption}
	\label{my-label}
	\begin{tabular}{|c|c|c|c|c|c|c|c|c|c|c|c|c|c|l|}
		\hline
		\multirow{2}{*}{Classifier} & \multirow{2}{*}{\#Est.} & \multirow{2}{*}{Max D} & \multirow{2}{*}{Min S} & \multirow{2}{*}{Min L} & \multirow{2}{*}{Max F} & \multicolumn{4}{c|}{10- Fold Cross Validation} & \multicolumn{5}{c|}{30\% Unseen Data (2-Fold Cross Validation)} \\ \cline{7-15} 
		&                         &                        &                        &                        &                        & Prec.      & Recall      & F1       & Time     & Prec.  & Recall  & F1    & t      & t                           \\ \hline
		\multirow{5}{*}{R}          & 5                       & 20                     & 10                     & 5                      & Sqrt(\#Features)       & 95\%       & 96\%        & 95\%     & 5.6      & 94\%   & 96\%    & 95\%  & 0.43 s & 0.06 s                      \\ \cline{2-15} 
		& 5                       & --                     & --                     & --                     & --                     & 95\%       & 97\%        & 96\%     & 5.7      & 95\%   & 96\%    & 95\%  & 0.49 s & 0.09 s                      \\ \cline{2-15} 
		& 10                      & 10                     & 20                     & 5                      & Sqrt(\#Features)       & 94\%       & 95\%        & 95\%     & 8.1      & 95\%   & 95\%    & 95\%  & 0.82s  & 0.084 s                     \\ \cline{2-15} 
		& 50                      & 5                      & --                     & --                     & Sqrt(\#Features)       & 95\%       & 86\%        & 90\%     & 26.1     & 95\%   & 85\%    & 90\%  & 2.81 s & 0.18 s                      \\ \cline{2-15} 
		& 25                      & 10                     & --                     & --                     & Sqrt(\#Features)       & 95\%       & 96\%        & 96\%     & 17.5     & 95\%   & 95\%    & 95\%  & 1.83 s & 0.126 s                     \\ \hline
		\multirow{6}{*}{A}          & 5                       & 20                     & 10                     & 5                      & Sqrt(\#Features)       & 95\%       & 97\%        & 96\%     & 7.9      & 94\%   & 97\%    & 95\%  & 0.77 s & 0.09 s                      \\ \cline{2-15} 
		& 5                       & --                     & --                     & --                     & --                     & 95\%       & 97\%        & 96\%     & 24.4     & 94\%   & 96\%    & 95\%  & 2.92 s & 0.088 s                     \\ \cline{2-15} 
		& 5                       & 10                     & 20                     & 5                      & Sqrt(\#Features)       & 95\%       & 97\%        & 96\%     & 7.4      & 94\%   & 96\%    & 95\%  & 0.77 s & 0.09 s                      \\ \cline{2-15} 
		& 10                      & 10                     & 20                     & 5                      & Sqrt(\#Features)       & 95\%       & 97\%        & 96\%     & 13       & 94\%   & 96\%    & 95\%  & 1.32 s & 0.12 s                      \\ \cline{2-15} 
		& 50                      & 5                      & --                     & --                     & Sqrt(\#Features)       & 95\%       & 97\%        & 96\%     & 42.1     & 94\%   & 96\%    & 95\%  & 4.66 s & 0.31 s                      \\ \cline{2-15} 
		& 25                      & 10                     & --                     & --                     & Sqrt(\#Features)       & 95\%       & 97\%        & 96\%     & 27.4     & 94\%   & 97\%    & 95\%  & 3.27 s & \multicolumn{1}{c|}{0.23 s} \\ \hline
	\end{tabular}
\end{table}


\begin{table*}[t]
	\centering
	\caption{Performance of Shortlisted Classifiers on 100 Thousand Rows Sample}
	\label{my-label}
	\begin{center}
	\begin{tabular}{|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|}
		\hline
		\multirow{2}{*}{Classifier} & \multirow{2}{*}{\#Est.} & \multirow{2}{*}{Max\textsubscript{d}} & \multirow{2}{*}{Min\textsubscript{s}} & \multirow{2}{*}{Min\textsubscript{l}} & \multirow{2}{*}{Max\textsubscript{f}} & \multicolumn{4}{c|}{10- Fold Cross Validation} & \multicolumn{5}{c|}{30\% Unseen Data} \\ \cline{7-15}
		&                         &                        &                        &                        &                        & Prec.      & Recall      & F1       & Time     & Prec.  & Recall  & F1    & T\textsubscript{L}      & T\textsubscript{P}                           \\ \hline
		\multirow{5}{*}{Random \linebreak Forest}          & 5                       & 20                     & 10                     & 5                      & \sqrt{\#f}      & 95\%       & 96\%        & 95\%     & 5.6      & 94\%   & 96\%    & 95\%  & 0.4 & 0.06                      \\ \cline{2-15}
		& 5                       & --                     & --                     & --                     & --                     & 95\%       & 97\%        & 96\%     & 5.7      & 95\%   & 96\%    & 95\%  & 0.5 & 0.09                      \\ \cline{2-15}
		& 10                      & 10                     & 20                     & 5                      & \sqrt{\#f}       & 94\%       & 95\%        & 95\%     & 8.1      & 95\%   & 95\%    & 95\%  & 0.8  & 0.08                     \\ \cline{2-15}
		& 50                      & 5                      & --                     & --                     & \sqrt{\#f}      & 95\%       & 86\%        & 90\%     & 26.1     & 95\%   & 85\%    & 90\%  & 2.8 & 0.2                      \\ \cline{2-15}
		& 25                      & 10                     & --                     & --                     & \sqrt{\#f}       & 95\%       & 96\%        & 96\%     & 17.5     & 95\%   & 95\%    & 95\%  & 1.83 s & 0.126 s                     \\ \hline
		\multirow{6}{*}{AdaBoost}          & 5                       & 20                     & 10                     & 5                      & \sqrt{\#f}      & 95\%       & 97\%        & 96\%     & 7.9      & 94\%   & 97\%    & 95\%  & 0.77 s & 0.09 s                      \\ \cline{2-15}
		& 5                       & --                     & --                     & --                     & --                     & 95\%       & 97\%        & 96\%     & 24.4     & 94\%   & 96\%    & 95\%  & 2.92 s & 0.088 s                     \\ \cline{2-15}
		& 5                       & 10                     & 20                     & 5                      & \sqrt{\#f}       & 95\%       & 97\%        & 96\%     & 7.4      & 94\%   & 96\%    & 95\%  & 0.77 s & 0.09 s                      \\ \cline{2-15}
		& 10                      & 10                     & 20                     & 5                      & \sqrt{\#f}       & 95\%       & 97\%        & 96\%     & 13       & 94\%   & 96\%    & 95\%  & 1.32 s & 0.12 s                      \\ \cline{2-15}
		& 50                      & 5                      & --                     & --                     & \sqrt{\#f}      & 95\%       & 97\%        & 96\%     & 42.1     & 94\%   & 96\%    & 95\%  & 4.66 s & 0.31 s                      \\ \cline{2-15}
		& 25                      & 10                     & --                     & --                     & \sqrt{\#f}       & 95\%       & 97\%        & 96\%     & 27.4     & 94\%   & 97\%    & 95\%  & 3.3 & \multicolumn{1}{c|}{0.2} \\ \hline
	\end{tabular}
	\end{center}\mbox{}
\end{table*}

%\begin{table*}
%	\centering
%	\caption{Performance of Shortlisted Classifiers on 100 Thousand Rows Sample}
%	\label{shortlist100k}
%	\begin{tabularx}{\textwidth}{|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|}
%		\hline
%		\multirow{2}{*}{Classifier} & \multirow{2}{*}{\#Est.} & \multirow{2}{*}{Max\textsubscript{d}} & \multirow{2}{*}{Min\textsubscript{s}} & \multirow{2}{*}{Min\textsubscript{l}} & \multirow{2}{*}{Max\textsubscript{f}} & \multicolumn{4}{c|}{10- Fold Cross Validation} & \multicolumn{5}{c|}{30\% Unseen Data} \\ \cline{7-15} 
%		&                         &                        &                        &                        &                        & Prec.      & Recall      & F1       & Time     & Prec.  & Recall  & F1    & T\textsubscript{L}      & T\textsubscript{P}                           \\ \hline
%		\multirow{5}{*}{Random \linebreak Forest}          & 5                       & 20                     & 10                     & 5                      & \sqrt{\#f}      & 95\%       & 96\%        & 95\%     & 5.6      & 94\%   & 96\%    & 95\%  & 0.4 & 0.06                      \\ \cline{2-15} 
%		& 5                       & --                     & --                     & --                     & --                     & 95\%       & 97\%        & 96\%     & 5.7      & 95\%   & 96\%    & 95\%  & 0.5 & 0.09                      \\ \cline{2-15} 
%		& 10                      & 10                     & 20                     & 5                      & \sqrt{\#f}       & 94\%       & 95\%        & 95\%     & 8.1      & 95\%   & 95\%    & 95\%  & 0.8  & 0.08                     \\ \cline{2-15} 
%		& 50                      & 5                      & --                     & --                     & \sqrt{\#f}      & 95\%       & 86\%        & 90\%     & 26.1     & 95\%   & 85\%    & 90\%  & 2.8 & 0.2                      \\ \cline{2-15} 
%		& 25                      & 10                     & --                     & --                     & \sqrt{\#f}       & 95\%       & 96\%        & 96\%     & 17.5     & 95\%   & 95\%    & 95\%  & 1.83 s & 0.126 s                     \\ \hline
%		\multirow{6}{*}{AdaBoost}          & 5                       & 20                     & 10                     & 5                      & \sqrt{\#f}      & 95\%       & 97\%        & 96\%     & 7.9      & 94\%   & 97\%    & 95\%  & 0.77 s & 0.09 s                      \\ \cline{2-15} 
%		& 5                       & --                     & --                     & --                     & --                     & 95\%       & 97\%        & 96\%     & 24.4     & 94\%   & 96\%    & 95\%  & 2.92 s & 0.088 s                     \\ \cline{2-15} 
%		& 5                       & 10                     & 20                     & 5                      & \sqrt{\#f}       & 95\%       & 97\%        & 96\%     & 7.4      & 94\%   & 96\%    & 95\%  & 0.77 s & 0.09 s                      \\ \cline{2-15} 
%		& 10                      & 10                     & 20                     & 5                      & \sqrt{\#f}       & 95\%       & 97\%        & 96\%     & 13       & 94\%   & 96\%    & 95\%  & 1.32 s & 0.12 s                      \\ \cline{2-15} 
%		& 50                      & 5                      & --                     & --                     & \sqrt{\#f}      & 95\%       & 97\%        & 96\%     & 42.1     & 94\%   & 96\%    & 95\%  & 4.66 s & 0.31 s                      \\ \cline{2-15} 
%		& 25                      & 10                     & --                     & --                     & \sqrt{\#f}       & 95\%       & 97\%        & 96\%     & 27.4     & 94\%   & 97\%    & 95\%  & 3.3 & \multicolumn{1}{c|}{0.2} \\ \hline
%	\end{tabularx}
%\end{table*}


%\begin{figure*}[!t]
%\begin{table*}[t]
%	\centering
%	\caption{Performance of Shortlisted Classifiers on 100 Thousand Rows Sample}
%	\label{my-label}
%	\begin{tabular}{|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|}
%		\hline
%		\multirow{2}{*}{Classifier} & \multirow{2}{*}{\#Est.} & \multirow{2}{*}{Max\textsubscript{d}} & \multirow{2}{*}{Min\textsubscript{s}} & \multirow{2}{*}{Min\textsubscript{l}} & \multirow{2}{*}{Max\textsubscript{f}} & \multicolumn{4}{c|}{10- Fold Cross Validation} & \multicolumn{5}{c|}{30\% Unseen Data} \\ \cline{7-15} 
%		&                         &                        &                        &                        &                        & Prec.      & Recall      & F1       & Time     & Prec.  & Recall  & F1    & T\textsubscript{L}      & T\textsubscript{P}                           \\ \hline
%		\multirow{5}{*}{Random \linebreak Forest}          & 5                       & 20                     & 10                     & 5                      & \sqrt{\#f}      & 95\%       & 96\%        & 95\%     & 5.6      & 94\%   & 96\%    & 95\%  & 0.4 & 0.06                      \\ \cline{2-15} 
%		& 5                       & --                     & --                     & --                     & --                     & 95\%       & 97\%        & 96\%     & 5.7      & 95\%   & 96\%    & 95\%  & 0.5 & 0.09                      \\ \cline{2-15} 
%		& 10                      & 10                     & 20                     & 5                      & \sqrt{\#f}       & 94\%       & 95\%        & 95\%     & 8.1      & 95\%   & 95\%    & 95\%  & 0.8  & 0.08                     \\ \cline{2-15} 
%		& 50                      & 5                      & --                     & --                     & \sqrt{\#f}      & 95\%       & 86\%        & 90\%     & 26.1     & 95\%   & 85\%    & 90\%  & 2.8 & 0.2                      \\ \cline{2-15} 
%		& 25                      & 10                     & --                     & --                     & \sqrt{\#f}       & 95\%       & 96\%        & 96\%     & 17.5     & 95\%   & 95\%    & 95\%  & 1.83 s & 0.126 s                     \\ \hline
%		\multirow{6}{*}{AdaBoost}          & 5                       & 20                     & 10                     & 5                      & \sqrt{\#f}      & 95\%       & 97\%        & 96\%     & 7.9      & 94\%   & 97\%    & 95\%  & 0.77 s & 0.09 s                      \\ \cline{2-15} 
%		& 5                       & --                     & --                     & --                     & --                     & 95\%       & 97\%        & 96\%     & 24.4     & 94\%   & 96\%    & 95\%  & 2.92 s & 0.088 s                     \\ \cline{2-15} 
%		& 5                       & 10                     & 20                     & 5                      & \sqrt{\#f}       & 95\%       & 97\%        & 96\%     & 7.4      & 94\%   & 96\%    & 95\%  & 0.77 s & 0.09 s                      \\ \cline{2-15} 
%		& 10                      & 10                     & 20                     & 5                      & \sqrt{\#f}       & 95\%       & 97\%        & 96\%     & 13       & 94\%   & 96\%    & 95\%  & 1.32 s & 0.12 s                      \\ \cline{2-15} 
%		& 50                      & 5                      & --                     & --                     & \sqrt{\#f}      & 95\%       & 97\%        & 96\%     & 42.1     & 94\%   & 96\%    & 95\%  & 4.66 s & 0.31 s                      \\ \cline{2-15} 
%		& 25                      & 10                     & --                     & --                     & \sqrt{\#f}       & 95\%       & 97\%        & 96\%     & 27.4     & 94\%   & 97\%    & 95\%  & 3.3 & \multicolumn{1}{c|}{0.2} \\ \hline
%	\end{tabular}
%	\begin{tablenotes}
%		\small
%		\item[*]
%		\begin{tabular}{ll}
%			d & h
%		\end{tabular} 
%	\end{tablenotes}
%\end{table}


%\hrulefill
%\vspace*{4pt}
%\end{figure*}


		\label{sec:eval}
		\subsection{Evaluation}
		
		