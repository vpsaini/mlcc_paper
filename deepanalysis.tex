\subsection{Analysis of Clone Pairs}
\label{sec:deepanalysis}

After looking at cloned and non-cloned methods individually, we wanted
to find out more about the clones, and why they were being identified
as such. Are they auto generated or copy-pasted then modified? Are
there any signs that could tell us that developers were aware of a
method before they decided to create a clone of it? This required us
to do a deeper analysis of clone methods where we not only looked at
the code of the clone pairs, but also the Java files they come
from. To this end, we analyzed $32$ randomly selected clone pairs in
search for the type of cloning among the pairs of methods. The number $32$ was selected arbitrarily.
		
\begin{table}
\begin{center}
\begin{tabular}{ccc|cc}
Type-1 & Type-2 & Type-3 & Same File & A.Gen. \\ \hline
4 & 16 & 12 & 13 & 5  \\
					
\end{tabular}
\caption{Results: analysis of 32 clone pairs}
\label{tab:deepAnalysis}
\end{center}
\end{table}

Table~\ref{tab:deepAnalysis} shows the results of this analysis. The
table has two sides, to distinguish between the type of cloning (1, 2
or 3) and the two additional characteristics we found interesting:
whether the methods were found in the same file or not, and whether
they were automatically generated or not.

As expected we found a variety of types of cloning, from identical
(Type-1) to less identical (Type-3). There were 13 instances where
both members of a clone pair have the same Java source file. We looked
into the source file for each clone pair and found that in 7 out of 13 cases these methods appear consecutively next to each
other. Also, none of the 13 methods appear to be auto generated. We infer from the above two observations that the developers were possibly aware of the existence of these 7 similar methods. However, a more in-depth analysis of version history of these files would be required to conclude with confidence that the developers were aware of these similar methods.

In all of these cases of same-file clones, separation of concerns
seemed to be the design rationale that the developers chose to
follow. For example, consider the clone pairs shown in
Listing~\ref{lst:sepofconcerns}. The method on the
top, \textit{expandItemsRecursively}, is structurally very similar to
the method at the
bottom, \textit{collapseItemsRecursively}. The \textit{expandItemsRecursively}
method calls \textit{expandItem} inside the \textit{while} loop,
whereas the other method calls \textit{collapseItem} method inside
the \textit{while} loop. Also, there is an additional call
to \textit{requestRepaint} method made inside
the \textit{expandItemsRecursively} method. Functionally these two
methods are performing the opposite functions, but structurally these
are Type-3 clones. The developers chose to implement the two functions
separately in two different methods instead of abstracting the
commonalities into a third method.

    \begin{minipage}{0.95\textwidth}
    \begin{lstlisting}[label={lst:sepofconcerns},caption={Type-3 clones, demonstrating separation of concerns}
    ]
    public boolean expandItemsRecursively(Object startItemId){
      boolean result = true;
      // Initial stack
      final Stack<Object> todo = new Stack<Object>();
      todo.add(startItemId);
      // Expands recursively
      while (!todo.isEmpty()){
        final Object id = todo.pop();
        if (areChildrenAllowed(id) && ! (*@ \hl{expandItem(id, false)}  @*) ){
          result = false;
        }
        if (hasChildren(id)){
          todo.addAll(getChildren(id));
        }
      }
      (*@ \hl{requestRepaint();} @*)
      return result;
    }
    \end{lstlisting}
    \begin{lstlisting}
     public boolean collapseItemsRecursively(Object startItemId){
       boolean result = true;
       // Initial stack
       final Stack<Object> todo = new Stack<Object>();
       todo.add(startItemId);
       // Collapse recursively
       while (!todo.isEmpty()) {
         final Object id = todo.pop();
         if (areChildrenAllowed(id) && ! (*@ \hl{collapseItem(id)}  @*)){
           result = false;
         }
         if (hasChildren(id)) {
           todo.addAll(getChildren(id));
         }
       }
       return result;
     }
     \end{lstlisting}    
     \end{minipage}

We observed 5 instances where the methods in the clone pairs were
generated using some code generator. In all such instances the members
of the clone pair were found in different Java source files. All of
these clone pairs were Type-2 clones. Listing~\ref{lst:autogentcode}
shows a clone pair that was generated using JavaTM Architecture for
XML Binding (JAXB). Both methods look very similar in structure and
functionality. Each method copies an object of a different type to
another object. We looked at the source files for these two methods; the method on the top comes from a file named \textit{FeatureListURL.java} and the method on the bottom comes from a file named \textit{StyleSheetURL.java}. We found comments in both of these files, shown in Listing~\ref{lst:autogencomments}, indicating that both these files were auto-generated.

\begin{minipage}{0.95\textwidth}
	\begin{lstlisting}[label={lst:autogencomments},caption={commnets taken from StyleSheetURL.java indicating that the source file are auto-generated}
	]  
//
// (*@\hl{This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation }@*), vhudson-jaxb-ri-2.1-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2011.02.20 at 12:49:02 PM MEZ 
//

\end{lstlisting}
\end{minipage}

One could write a generic method to avoid creation of
clones, but since these are generated methods, maintainability may not
be important. Should there be any need of bug fixes or modifications,
the developers can modify the code generator and all members of the
clone groups will get updated.

\begin{minipage}{0.95\textwidth}
\begin{lstlisting}[label={lst:autogentcode},caption={Type-2 clones, demonstrating generated methods}
]
public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
  final Object draftCopy = ((target == null)?createNewInstance():target);
  if (draftCopy instanceof (*@\hl{FeatureListURL}@*)) {
    final (*@\hl{FeatureListURL}@*) copy = (((*@\hl{FeatureListURL}@*)) draftCopy);
    if (this.format!= null) {
      String sourceFormat;
      sourceFormat = this.getFormat();
      String copyFormat = ((String) strategy.copy(LocatorUtils.property(locator, "format", sourceFormat), sourceFormat));
      copy.setFormat(copyFormat);
    } else {
      copy.format = null;
    }
    if (this.value!= null) {
      String sourcevalue;
      sourcevalue = this.getvalue();
      String copyvalue = ((String) strategy.copy(LocatorUtils.property(locator, "value", sourcevalue), sourcevalue));
      copy.setvalue(copyvalue);
    } else {
      copy.value = null;
    }
  }
  return draftCopy;
}
\end{lstlisting}
\begin{lstlisting}
public Object copyTo(ObjectLocator locator, Object target, CopyStrategy strategy) {
  final Object draftCopy = ((target == null)?createNewInstance():target);
  if (draftCopy instanceof (*@\hl{StyleSheetURL}@*)) {
    final (*@\hl{StyleSheetURL}@*) copy = (((*@\hl{StyleSheetURL}@*)) draftCopy);
    if (this.format!= null) {
      String sourceFormat;
      sourceFormat = this.getFormat();
      String copyFormat = ((String) strategy.copy(LocatorUtils.property(locator, "format", sourceFormat), sourceFormat));
      copy.setFormat(copyFormat);
    } else {
      copy.format = null;
    }
    if (this.value!= null) {
      String sourcevalue;
      sourcevalue = this.getvalue();
      String copyvalue = ((String) strategy.copy(LocatorUtils.property(locator,"value", sourcevalue), sourcevalue));
      copy.setvalue(copyvalue);
    } else {
      copy.value = null;
    }
  }
return draftCopy;
}
\end{lstlisting} 
\end{minipage}

\subsection{Analysis of Clone Groups}

In order to understand why there are more small methods, we turned to analyzing clone groups, i.e. the set of methods that were identified as clones of
a common method. To this end, we divided the cloned methods from
subsection~\ref{sec:analysismethods} into two subsets: i) set of
cloned methods which are very small (\textit{VS} and small
\textit{S}); and ii) the set of cloned methods which are large
(\textit{L} and very large \textit{VL}). For each method in these two sets, we created a set of methods that were reported as its clones. This gave us the clone groups for each method. We named the clone groups obtained from smaller (\textit{VS} and \textit{S}) methods as \textit{Clone Groups with Small Methods}. Conversely, we named the clone groups obtained from the larger (\textit{L} and \textit{VL}) as \textit{Clone Groups with Large Methods}. We then analyzed these groups looking for differences and similarities in these groups.

Figure~\ref{fig:clonegroupsdataset} shows the distribution of clone groups in the entire dataset. The X-axis in the left-histogram represents the number of members in the clone groups. Whereas the X-axis in the right-histogram has been transformed into the $\log_{10}$ scale. The Y-axis, in both histograms, represents the percentage of clone groups. 

\begin{figure}
	\centering
	\includegraphics[width=13cm]{plots/nooverloaded_no_nos1/cg.pdf}
	\caption {\scriptsize Distribution of clone groups. Left: histogram where number of members in a clone group are shown in linear scale(X-axis). Right:histogram where number of members in a clone group are shown in $\log_{10}$ scale(X-axis)}
	\label{fig:clonegroupsdataset}
	\vspace{-5mm}
\end{figure}

To get a better picture of the distribution, we are showing the histogram in linear and log scale. We call clone groups with 2 to 10 members \textit{Small clone groups}, clone groups with 11 to 100 members as \textit{Medium clone groups}, clone groups with 101 to 1000 as \textit{Large clone groups}, and clone groups with more than 1000 members are \textit{Very large clone groups}. The authors decided and agreed upon these thresholds based on human intuitions.

There are approximately 60\% of the clone groups fall into \textit{Small clone group} ($\log_{10} 0.5 = 3.16$,$\log_{10}1.5 = 31.6$, $\log_{10}2.5 = 316.22$, and $\log_{10}3.5 = 3162.2$); around 20\% in \textit{Medium clone group}; and around 20\% in \textit{Large} and \textit{Very clone groups} combined. We note that these clone groups should not be confused with clone classes. In a clone class, all members of the class are reported to be clones of each other, whereas in a clone group, all members of the group are reported to be clones of one method. It could be possible that some or all of the members of a clone group could be clones of each other, but it is not a necessary condition. We did not compute clone classes because computing clone classes is not trivial and SourcererCC currently does not support reporting clone classes.

\pagebreak

\subsubsection{Clone Groups with Small Methods}
\label{sec:cg_smaller}

While looking at the clone groups we saw some \textit{large clone groups} ($>$100 methods in a group), hinting towards the use of code generators. We looked at the Java source files for some of the methods, specifically looking for comments indicating the use of code generators; we found many instances where the methods were auto generated. We noticed some patterns: i) the large clone groups usually indicate the use of a code generator; ii) the cloned methods which are auto generated usually exhibit Type-2 cloning, however we did find some instances of Type-3 clones as well; and iii) smaller auto-generated methods show more instance of Type-2 cloning than Type-3, which are more prevalent in larger methods.
 
Table~\ref{tab:cg_smaller} shows some of the largest clone groups that
we observed. Column \textit{Project} shows name of the project where
the clone group was found; column \textit{Count} shows the number of
methods present in the group; column \textit{Type} shows the type of
clones we observed (Type-1, Type-2, and Type-3); the last column, \textit{A.Gen.} shows whether the methods in the clone group are auto generated or not; the Value \textit{?}, means
that we are not sure, in the absence of any reliable indicators, if the members were auto generated.

\begin{table}
	\begin{center}
		\begin{tabular}{l*{4}{c}r}
			Project & Count & Type & A.Gen. \\ 
			\hline
			jython-standalone$@$2.5.2 & 5,474 & 2 & Yes \\
			hapi-osgi-base$@$2.0-beta1 & 4,421 & 2,3 & Yes \\
			geronimo-schema-jee\_5$@$1.2 & 2,974 & 2 & Yes \\
			geronimo-schema-javaee\_6$@$1.0 & 2,288 & 2 & Yes \\
			cxf-testutils$@$2.6.0 & 2,077 & 2 & Yes \\
			portal-service$@$6.1.0 & 1,125 & 2,3 & ? \\
			cmlxom$@$3.1  & 880 & 2 & Yes \\
			openejb-itests-client$@$4.0.0-beta-2  & 575 & 2 & ? \\
			aws-java-sdk$@$1.3.8  & 378 & 2 & Yes \\
			dsl-pom$@$5.0.0 & 126 & 2 & Yes \\
			trove4j$@$3.0.2 & 126 & 2 & ? \\
			axis2-adb$@$1.6.2  & 94 & 1 & ? \\
			
		\end{tabular}
		\caption{Clone groups of smaller (\textit{VS} and \textit{S}) methods}
		\label{tab:cg_smaller}
	\end{center}
	\vspace{-5mm}
\end{table}

As the table shows, most of the large clone groups that we saw were
auto generated, which, in retrospect, is not surprising. Row 1 in
Table~\ref{tab:cg_smaller} shows one large clone group with
$5,474$ members. This clone group belongs to jython-standalone$@$2.5.2
project. We looked at a around 100 random methods of
this clone group and they all were auto generated and showed Type-2
cloning. Listing~\ref{lst:jython} shows an example pair for this clone
group.

\begin{minipage}{0.95\textwidth}
\begin{lstlisting}[label={lst:jython},caption={An example clone pair from jython-standalone$@$2.5.5 project}
]
public PyObject __(*@\hl{add}@*)__(PyObject other) {
	PyType self_type=getType();
	PyObject impl=self_type.lookup("__(*@\hl{add}@*)__");
	if (impl!=null) {
		PyObject res=impl.__get__(this,self_type).__call__(other);
		if (res==Py.NotImplemented)
			return null;
		return res;
	}
	return super.__(*@\hl{add}@*)__(other);
}
\end{lstlisting}
\begin{lstlisting}
public PyObject __(*@\hl{radd}@*)__(PyObject other) {
	PyType self_type=getType();
	PyObject impl=self_type.lookup("__(*@\hl{radd}@*)__");
	if (impl!=null) {
		PyObject res=impl.__get__(this,self_type).__call__(other);
		if (res==Py.NotImplemented)
			return null;
		return res;
	}
	return super.__(*@\hl{radd}@*)__(other);
}
\end{lstlisting}
\end{minipage}

Similarly, in other large clone groups (Table~\ref{tab:cg_smaller}),
we observed that code generated using templates is one of the major
causes behind the structural similarities in the members of large
clone groups.

\subsubsection{Clone Groups with Large Methods}

Table~\ref{tab:cg_larger} shows some of the clone groups that we
observed in the set of large (\textit{L} and very large \textit{VL})
cloned methods. Structurally, the table is identical to the
Table~\ref{tab:cg_smaller}.

We observed a pattern similar to what we observed in
Section~\ref{sec:cg_smaller} -- the larger clone groups contain methods that are auto generated. 
Row 1 in Table~\ref{tab:cg_larger} shows a large clone group (2,345 members). The members of this group are auto generated using \textit{JacORB IDL compiler}, as mentioned in their Java source files. Similarly, we found other clone groups where the cloning can be associated to code generators. Unlike our observations in Section~\ref{sec:cg_smaller}, where Type-2 dominated the clones in larger clone groups, we saw both, Type-2 and Type-3 clones as shown in Table~\ref{tab:cg_larger}.

\begin{table}
	\begin{center}
		\begin{tabular}{l*{4}{c}r}
			Project & Count & Type & A.Gen. \\ 
			\hline
			jacorb$@$2.3.1 & 2,345 & 2,3 & Yes \\
			iso-19139-d\_2006\_05\_04-schema$@$1.0.3 & 209 & 2,3 & Yes \\
			sfee$@$1.0.4 & 165 & 2,3 & Yes \\
			esper$@$4.6.0 & 21 & 2 & Yes \\
			jackrabbit-spi-commons$@$2.4.1 & 17 & 2,3 & Yes \\
			portal-impl$@$6.1.0 & 4 & 3 & No \\
		\end{tabular}
		\caption{Clone groups of larger (\textit{L} and \textit{VL}) methods}
		\label{tab:cg_larger}
	\end{center}
\end{table}

\subsubsection{Small vs. Large Cloned Methods}

Comparing tables \ref{tab:cg_smaller} and \ref{tab:cg_larger}, it
appears that methods which are automatically generated using some
template, and that end up being very similar, tend to be small rather
than large. Table~\ref{tab:cg_smaller}, with the small methods, shows
many very large clone groups with more than 1,000 methods, whereas
Table~\ref{tab:cg_larger} shows only one clone group with more than
1,000 methods.

\begin{comment}


\subsection{More on Clone Groups}

We wanted to know what is the distribution of clone groups in our dataset. To this end we plotted their histograms.  Figure~\ref{fig:clonegroupsdataset} shows the distribution of clone groups in our dataset. The X-axis in the left-histogram represents the number of members in the clone groups. Whereas the X-axis in the right-histogram has been transformed into the $\log_{10}$ scale. The Y-axis, in both histograms, represents the percentage of clone groups. 

\begin{figure}
	\centering
	\includegraphics[width=13cm]{plots/nooverloaded_no_nos1/cg.pdf}
	\caption {\scriptsize Distribution of clone groups. Left: histogram where number of members in a clone group are shown in linear scale(X-axis). Right:histogram where number of members in a clone group are shown in $\log_{10}$ scale(X-axis)}
	\label{fig:clonegroupsdataset}
	\vspace{-5mm}
\end{figure}

The histogram on the left does not provide a clear picture of the clone groups; so we plotted the histogram with X-axis transformed into $\log_{10}$ scale. Around 35\% of the clone groups have 2 to 3 members ($\log_{10} 0.5 = 3.16$). 
\end{comment}

\subsection{Summary of Mixed Method Analysis Findings}

To understand why cloned methods, on average, were found to
be smaller than the non-cloned methods we performed the mixed method analysis. We also compared the two types of methods based on their \textit{comprehensibility} and \textit{usefulness of comments} found in them. Additionally we looked into whether cloned and non-cloned methods perform similar functionalities or not. 

Our analysis allowed us to gain some additional insights as to \textit{why} cloned
methods in our dataset are smaller than non-cloned methods. We found
one possible factor that may lead to the size differences -- Among the methods identified as clones, a very large number of them seem to be automatically generated. We found many very large clone groups with more than 1,000 methods each. Furthermore, it seems that these automatically generated methods tend to be smaller rather than larger. This may explain why so many more cloned methods are small, rather than following the size distribution of their non-clone counterparts.

The analysis pointed out to the presence of bodiless methods (\textit{interfaces}) in our dataset. This could have impacted our findings; so we performed the quantitative analysis (see Section~\ref{sec:nos1}) again after removing such methods and their clones from the dataset. The results did not change our findings; ensuring that the number of such bodiless methods were not enough to impact the findings of this study.

We did not observe any statistically significant difference between cloned and non-cloned methods with respect to the \textit{comprehensibility} and \textit{usability of comments}. 

We also found that certain functionality (e.g. parsing and building strings) tends to be more pervasive in cloned methods than non-cloned methods, suggesting that these kinds of functionality often come in multiple, but similar, flavors for the same objects.

Additionally, we gained some more insights about cloned
methods. Specifically, among the clones that don't seem to be
automatically generated, we found many instances of multi-purpose
methods, i.e. methods that have very similar structure and vocabulary
but do the opposite (expand vs. collapse, etc.). 


