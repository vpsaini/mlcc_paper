\section{Evaluation} \label{sec:evaluation}

We measure the recall of \Name\ using
Big-CloneEval~\cite{svajlenko2016bigcloneeval}. Big-CloneEval is a
very good tool for performing clone detection tool evaluation
experiments with BigCloneBench~\cite{bcb_icsme15} clone
benchmark. Big-CloneEval reports recall numbers for Type-1 (T1),
Type-2 (T2), Type-3, and Type-4 clones.

To report numbers for Type-3 and Type-4 clones, the tool categorizes
Type-3 and Type-4 clones into four categories based on their
syntactical similarity, as follows. Very Strongly Type- 3 (VST3)
clones have a syntactical similarity between 90\% (inclusive) and
100\% (exclusive), Strongly Type-3 (ST3) in 70-90\%, Moderately Type-3
(MT3) in 50-70\% and Weakly Type-3/Type-4 (WT3/4) in 0-50\%.

\subsection{Dataset}

For the reasons explained in Section~\ref{sec:train_process}, we couldn't
use the entire set of methods available for recall measurement. This
results in artificially lower recall numbers on the benchmark. To get
around this issue, rather than measuring recall directly on the
benchmark, we do a relative comparison of our recall numbers with that
of SourcererCC over the same (smaller) set of methods. In doing this,
we ignore the absolute recall numbers reported by BigCloneEval, but we
are able to get a relative recall figure with respect to SourcererCC. 

While preprocessing the methods from the benchmark, we remove all the
methods defined in inner or anonymous classes. We then
consider only those methods that are 50 tokens in length or
greater. This is the standard minimum clone size for measuring recall
[5, 36]. This results into a reduced-dataset of 168,835 methods. We
then run \Name\ and SourcererCC on these methods. Finally, using
Big-CloneEval we generate the recall evaluation reports for both.

\subsection{Configurations} \label{sec:config}

We configured SourcererCC to find clones at 70\% overlap
similarity. This configuration is recommended and tested by the
authors of SourcererCC to show state-of-the-art recall and precision
results. For our approach we configured the Action-filter at 75\%
overlap similarity. The threshold to create input partitions was set
to 70\%.
 
\begin{table*}[t]
	\centering
	\small
	\tabcolsep=0.075cm
	\centering
	\caption{BigCloneBench Recall Measurements}\label{tab:recall}
	\begin{tabular}{ccccccccccccccccccccc}
		\toprule
		\multirow{2}{*}{Tool} & \multicolumn{6}{c}{All Clones} & & \multicolumn{6}{c}{Intra-Project Clones} & & \multicolumn{6}{c}{Inter-Project Clones} \\
		\cmidrule{2-7}\cmidrule{9-15}\cmidrule{16-21}
		& T1  & T2 & VST3 & ST3 & MT3 & WT3/T4 & & T1  & T2 & VST3 & ST3 & MT3 & WT3/T4 & & T1  & T2 & VST3 & ST3 & MT3 & WT3/T4 \\
		\midrule
		SourcererCC  & 19,794 & \textbf{6,920}  & 1,803  & \textbf{5,087} & 1177  & 230 & & 4,151 & 6,256  & 931  & 1,920 & 914  & 219  & & 1,5643  & \textbf{664}  & 872  & \textbf{3,167}  & \textbf{263}  & 11 \\
		\midrule
		\Name & 19,794 & 6,914  & \textbf{1,807}  & 4905 & \textbf{3,413}  & \textbf{11,662} & & 4,151  & \textbf{6,262}  & \textbf{933} & \textbf{1,941} & \textbf{3,164}  & \textbf{1,163} & & 15,643 & 652  & \textbf{874}  & 2,964 & 249 & \textbf{29} \\
		\bottomrule
	\end{tabular}
%	\vspace{0.2in}
\end{table*}

\subsection{Recall}

On this reduced dataset, SourcererCC reported 706,488 clone pairs, and
\Name\ reported 813,546 clone pairs.  Table~\ref{tab:recall}
summarizes the recall number of clones reported by Big-CloneEval tool
for SourcererCC and \Name. The recall numbers are summarized per clone
category, all clones, intra and inter-project clones.

\begin{table}[t]
	\centering
	\scriptsize
	\tabcolsep=0.075cm
	\centering
	\caption{BigCloneBench Recall Measurements Taken from~\cite{sajnani2016sourcerercc}}\label{tab:bcb_recall_scc}
	\begin{tabular}{ccccccccc}
		\toprule
		\multirow{2}{*}{Tool} & \multicolumn{6}{c}{All Clones Recall in \%}  \\
		\cmidrule{2-7}
		& T1 (35,787)  & T2 (4,573) & VST3 (4,156) & ST3 (14,997) & MT3 (79,756) & WT3/T4 (7,729,291) \\
		\midrule
		SourcererCC & \textbf{100\%} & 98\%           & 93\%           & 61\%          & 5\%           & 0\%          \\
		CCFinderX  & \textbf{100\%} & 93\%           & 62\%           & 15\%          & 1\%           & 0\%          \\
		Deckard    & 60\%           & 58\%           & 62\%           & 31\%          & \textbf{12\%} & \textbf{1\%}  \\
		iClones    & \textbf{100\%} & 82\%           & 82\%           & 24\%          & 0\%           & 0\%           \\
		NiCad     & \textbf{100\%} & \textbf{100\%} & \textbf{100\%} & \textbf{95\%} & 1\%           & 0\%           \\
		\bottomrule
	\end{tabular}
%	\vspace{0.2in}
\end{table}


\Name~\ performs almost at par with SourcererCC on T1, T2, VST3, and
ST3 categories. However, \Name~\ performs significantly better on MT3 and
WT3/T4 categories. For MT3, our recall numbers are almost three
$(2.9)$ times more than SourcererCC. \Name's recall numbers for Type-4
clones is 50 times higher, which is expected as SourcererCC is not
designed to detect Type-4 clones.

To have a better idea about what these recall results really mean,
Table~\ref{tab:bcb_recall_scc} shows the recall numbers of various
tools evaluated by the SourcererCC authors.\footnote{The table is
  reproduced here with permission from the authors.} The numbers
written in the column headers are the total number of clone pairs for
the category. The details of their recall experiment can be found
elsewhere~\cite{sajnani2016sourcerercc}. While most of the tools
performed well for T1 and T2 categories, their performance on
categories MT3 and WTE/T4 is poor.  


\subsection{Precision I}\label{sec:precision1}

In the absence of any standard benchmark to measure precision of clone
detectors, we compared the precision of SourcererCC and
\Name\ manually, which is a common practice used in measuring code
clone detection tools precision.

\textbf{Methodology}. For both SourcererCC and \Name\ we randomly
selected 384 clone pairs, a statistically significant sample with 95\%
confidence level and 5\% confidence interval, from the clone pairs
they detected in the recall experiment. The validation of clones were
done by three judges, who are also the authors of this paper. The
judges were kept blind of the source of each clone pair. Unlike many
classification tasks that require fuzzy human judgement, this task
required following very strict definitions of what constitutes Type-1,
Type-2, Type-3, and Type-4 clones. Where there were conflicts in
judgement among the authors, they were resolved by discussions that
always ended up in consensus simply by invoking the definitions.

We found the precision of SourcererCC to be 100\% while precision for
\Name~\ was 90\%. The analysis of where \Name\ failed shed a light on
many opportunities for improving it. Specifically, we found that the
majority of false positives reported by \Name\ were method pairs that
had in common only one action: {\em length}. This semantic signal is
too weak for truly identifying semantic similarity, and we have some
ideas for how to avoid these false positives.


\subsection{Precision II}

To gain more confidence in our WT3/T4 results, we manually inspected
the clone pairs that were reported by us but not by SourcererCC. In
total there were 219,116 clone pairs which we found but SourcererCC
did not.  We took a random sample of 50 clones pairs that were
reported by us and not by SourcererCC. We then conducted a precision
study, similar to what we did in Section~\ref{sec:precision1}. In the
end, 45 out of these 50 clone pairs (90\%) were true clones that had
been missed by SourcererCC; most of them were WT3/T4, for which
SourcererCC was not designed to detect.

The recall and precision experiments show that our approach is at par
with SourcererCC, in terms of recall for Type-1, Type-2, and first two
categories of Type-3 clones. \Name~\ is significantly better to detect
harder-to-detect clones of categories MT3 and WT3/T4 while maintaining
am acceptable precision, which can be improved.

It is interesting to note here that we trained our models using
SourcererCC, a Type-3 clone detector, and we are able to detect
harder-to-find clones. This indicates that the features generated from
metrics can capture fine details in the difference and similarities of
two methods. This is a positive sign as it opens new possibilities for
harder-to-detect Type-3 and Type-4 clones.


%\end{minipage}
%\subsection{Precision III}
%We repeated the precision study on the clone pairs that were reported by SourcererCC and were considered as false-positives by our approach. Again, we sampled 50 clone pairs from the 

\subsection{Scalability}

As mentioned before, scalability is an important requirement of the
design of \Name. Most metrics-based clone detectors, including the
recent machine learning based one, tend to grow quadratically with the
size of input, which greatly limits the size of dataset to which they
can be applied.

We demonstrate the scalability of \Name\ in two parts. In the first
part, we show the impact of the two-level input partitioning and
Action Filter on the number of candidates to be processed. As a
reminder, reducing the number of candidates early on in the pipeline
greatly improves the scalability of clone detection tools. The second
part is a run time performance experiment.

\subsubsection{Number of candidates} 

To measure the impact of the Action Filter and two-level input
partitioning on the number of candidates, we selected 1,000 random
methods as queries from the dataset. We then executed these 1,000
queries on our system to see the impact of the Action filter and input
partitioning on the number of candidates to be sent to the
metrics-based cognitive component. The threshold for Action filter was
set to 75\%. Also we selected 7 as the number of partitions for input
partitioning.

Table~\ref{tab:candidates} summarizes the impact. The top row shows
the base line case where each query is paired with every method
in the dataset, except for itself. This is the {\em modus operandi} of
many clone detection tools. In the second row, the
Action-filter's similarity was set to 1\% to minimize it's impact,
however, partitions were turned on. For the results in third row, we
had kept the Action filter on at 75\% similarity threshold but we
switched off the partitioning. The bottom row shows the results for
number of candidates when both Action filter and input partitioning
were switched on. We can see that Action filter has a strong impact
on reducing the number of candidate pairs. The partitioning lets us do
in-memory computations; together they both help us achieve high
speed and scalability.

\begin{table}
	\scriptsize 
	\centering
	\tabcolsep=0.10cm
	\caption{Impact of Action Filter and Input Partitioning}
	\label{tab:candidates}
	\begin{tabular}{ccc}
		\toprule
		Action Filter & Input Partitioning & Num-Candidates \\
		\midrule
		No Filter & No partitions & 1,559,493,000 \\
		1\% & on & 934,014 \\
		75\% & No partitions & 37,697 \\
		75\% & on & 36,023 \\
		%				\cmidrule{2-7}
		%				& \multicolumn{6}{c}{7868560} \\
		\bottomrule
	\end{tabular}
	\vspace{-0.225in}
\end{table}

\subsubsection{Run time and resource demands} 

To test the performance of \Name\ on a large dataset, we downloaded
entire IJaDataset~\cite{ijadataset}, a large inter-project Java
repository containing 25,000 open-source projects (3 million source
files, 250MLOC) mined from SourceForge and Google Code. We extracted
the methods with 50 tokens or more. We removed the methods which are
defined inside the inner and anonymous classes as JHawk produces
inconsistent results on such methods. We then calculated the metrics
for the remaining methods in the dataset. JHawk failed to calculate
metrics for some methods, we are yet to identify the reason for
failure. In the end we were left with 1,559,494 methods (47MLOC). We
use the configuration as described in Section~\ref{sec:config}.  In
the absence of a proper tool for our approach, we did some ad-hoc
engineering to have two components. One to generate feature vectors
and other to consume these features and predict using the trained
model. The feature generation code is written in Java, which sends the
feature vectors using socket to the predictor, which is written in
Python. We set the maximum memory to be used by feature generator at
16G. The predictor on the other hand was set to use
maximum memory of 14G.

\Name~\ scaled to this input, taking 27 Hours to predict 49,686,581
clone pairs. We are confident that the execution time can be reduced
further by applying some good engineering optimizations to the tool --
something that has not been a priority yet.

Moreover, when we relaxed the memory constraint of the predictor to
80G and launched multiple threads for prediction, the process finished
in 8 hours, showing the highly parallelizable nature of \Name. The
feature generation can also be made parallel, as input is already
partitioned in self-contained portions. The partitions are mutually
exclusive and, therefore, multiple processes of feature generators can
be launched to achieve very high parallelization.


