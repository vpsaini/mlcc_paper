\section{Mixed Method Analysis}
\label{sec:qual}

The analysis we presented in the previous section points out that on an average there is a difference in the
size of cloned and the non-cloned methods. In the experiments where we did not control for size, the cloned and non-cloned
methods appear to be different with respect to the quality metrics. But, when we control for size (using Regression), the difference
disappears. The analyses raised more questions: 
Why are cloned methods, on an average smaller than the non-cloned methods?
Is the size difference between the cloned and non-cloned method apparent to a human observer? Do human observers see any difference in the \textit{comprehensibility} of cloned and non-cloned methods? Is there a difference in the \textit{usefulness of the comments} present in the cloned and non-cloned methods? Further, we wanted to understand if cloned and non-cloned methods perform fundamentally different functionalities? and Are they auto generated or copy-pasted then modified? Are there any signs that could tell us that developers were aware of a method before they decided to create a clone of it?

In order to answer the above questions we conducted a mixed method analysis. This analysis was made in three parts. We first looked at a
small subset of cloned and non-cloned methods in search for validation
of the quantitative results, as well as patterns and correlations
among different characteristics of the methods. On a second step, we
looked at pairs of clone methods. Finally, we looked at groups of
cloned methods. This section describes our analysis and
findings.

\textit{Methodology.} From the dataset we created two subsets, one with 150 randomly selected cloned methods
and other with 150 randomly selected non-cloned methods. Each author, three in total, manually inspected 50 cloned and 50 non-cloned methods. The analysis was made along four dimensions, namely \textit{Size},
\textit{Comprehensibility}, \textit{Usability of comments}, and
\textit{Functionality}. For each of the dimensions, the authors manually tagged their set of methods. One of the author's went though the entire 300 methods (150 cloned and 150 non-cloned) to cross-validate the tags. In the cases where there were conflicts, the authors resolved them by discussing the concern among each other followed by voting.

The conflicts occurred mostly in the \textit{Comprehensibility} and the \textit{Functionality} dimensions. For these two dimensions, we organized a calibration session to test the \textit{Inter-rater reliability}. In the calibration session each author independently looked at a common subset of 10 cloned and 10 non-cloned methods and re-tagged them. In both, \textit{Comprehensibility} and \textit{Functionality} dimensions for about 1 in 4 (25\%) cases we observed different judgments between any two authors. We note, for \textit{Comprehensibility} dimension, in the cases of conflicts, the authors were off by 1 point on the Likert Scale. That is if one author scored a method as 4 (hard to comprehend) then the author, in the case of conflict, would have scored that same method either as 3 (medium hard) or 5 (very hard). 

\textit{Test-rater reliability}. Further, many time later (4 months), the authors, for the \textit{Comprehensibility} dimension, re-tagged a random subset of 10 cloned and 10 non-cloned methods. These methods were selected from the author's respective set of 50 cloned and 50 non-cloned methods. We did this exercise to see if the authors are consistent in their judgments. We observed that the authors were consistent in about 7 out of the 10 cases, but when they were off, they were mostly off by 1 point on the Likert scale. To understand the process, consider a case where an author has been asked to score the \textit{Comprehensibility} of a set of methods, twice on a Likert scale. Once at time T1 and again at Time T2. The difference T2-T1 is about 4 months. The Likert scale uses options 1 to 5, where a score of 1 maps to the method being very easy to comprehend and a score of 5 maps to the method being very hard to understand. We observed that the author was mostly consistent, but when they were not, they were off by 1 point. 

To address the inconsistency issue, we grouped the Likert options during the analysis. For example, in the \textit{Comprehensibility} dimension, we grouped the scores of 1 (very easy) and 2 (easy) into 1' (easy). and the score of 4 (hard) and 5 (very hard) into 2' (hard). The score of 3 (medium hard) was removed from the analysis as it could fall into, owing to the off by 1 inconsistency, any of the original options 2 and 4.

The methodology presented here has elements of both qualitative and quantitative analysis, and hence it falls into the territory of the Mixed Methods Analysis~\cite{borrego2009quantitative}.  

\subsection{Analysis of Methods}
\label{sec:analysismethods}
		
\subsubsection{Size}
\label{sec:sizequal}

We wanted to understand whether a method looks very small,
medium, large, or very large to the observers. The Likert Scale uses
options 1 to 5, where the size increases as we move from option 1 to
option 5. Columns 2 to 6 in Table~\ref{tab:size} correspond to each
of these options. Column \textit{Type} contains the type of the
methods analyzed; column \textit{VS}, which corresponds to option 1 in
the Likert scale, shows the number of the methods that were very small
(3-8 \textit{NLOC}); column \textit{S}, (option 2) shows the number of
methods that small (9-20 \textit{NLOC}); column \textit{M} (option 3)
contains the number of methods that were of medium size (21-40
\textit{NLOC}); column \textit{L} (option 4) shows the number of
methods that were large (41-70 \textit{NLOC}); and column \textit{VL}
(option 5) shows the number of methods that were very large in size
($>$70 \textit{NLOC}). The above thresholds to categorize methods into different size options are selected using human intuition. The authors looked into a sample set of methods together and agreed upon the above thresholds. 
	
\begin{table}
\begin{center}
\begin{tabular}{l*{6}{c}r}
Type      & VS & S & M & L & VL \\ \hline
Clone     & 33 & 71 & 25 & 12 & 9 \\
Non-Clone & 30 & 54 & 42 & 10 & 14\\
\end{tabular}
\caption{Size of cloned and non-cloned methods}
\label{tab:size}
\end{center}
\end{table}

	
For size, we observed a noticeable difference in the number of methods
classified as small(\textit{S}) and very small (\textit{VS}), with cloned
methods being more often in these two categories. Conversely,
non-cloned methods were more often found to be very
large (\textit{VL}). 
%The observation confirms our findings in the
%quantitative study -- that there are more smaller methods in the set
%of clones.
To test whether the distribution of cloned and non-cloned methods across the size categories are statistically different, we conducted a Chi-Square test. The test result shows that the distributions are not statistically different. $\chi^2=8.0371, p=0.09023, df=4$ , where $\chi^2$ is the Chi-Square value, \textit{df} is the degrees of freedom, p is the P-value.

Further, we wanted to see whether there is any difference between the distribution of cloned and non-cloned methods if we categorize the methods into two categories: i) methods which are very small or small and ii) methods which are large or very large. To this end, we performed another Chi-Square test after combining \textit{VS} and \textit{S} into one category, and \textit{L} and \textit{VL} into another category. Again, we did not observe any statistically significant difference. $\chi^2=0.77287, p=0.3793, df=1$

The result of this test seems to be different than what we had observed in the quantitative analysis. The median \textit{NLOC} and median \textit{NOS} for cloned methods are 13 and 9, whereas median \textit{NLOC} and median \textit{NOS} for non-cloned methods are 16 and 11. The difference in both of these size metrics for cloned and non-cloned methods is about 18\%. But when the methods are categorized into different categories of size, the difference is not statistically significant. This suggests, the cloned and non-cloned methods do not seem to come from different distributions, when classified into the broad size-categories as generally viewed by human observers. 
	
Additionally, we also noticed that in three of the cloned methods in
our samples there were no method bodies but only method signatures,
meaning that these methods came from interfaces. However, they passed
the threshold for analysis, because they have more than 25 tokens --
these are methods with many parameters of complicated types. We note
that we had removed all methods with less than 25 tokens to ignore
empty methods, stubs, method contracts defined in interfaces, abstract
classes and other interferences. The heuristic did remove a many
insignificant methods, but not all of them, as seen
here. Listing~\ref{lst:interfacemethod} shows an example of one such
method signature. 

	\begin{minipage}{0.95\textwidth}
	\begin{lstlisting}[label={lst:interfacemethod},caption={Example of an empty method found during the qualitative study}
	]
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public com.liferay.portal.model.PersistedModel getPersistedModel(
	java.io.Serializable primaryKeyObj)
	throws com.liferay.portal.kernel.exception.PortalException,
	com.liferay.portal.kernel.exception.SystemException;
	\end{lstlisting}
	\end{minipage}

Upon further investigation, we found that for such methods JHawk
calculates \textit{NOS} as 1. There are 6,760 out of 412,705 cloned
methods (1.6\%) and 631 out of 616,604 non cloned methods (0.1\%) in
our dataset with \textit{NOS}$=$1. The presence of such methods could
partially explain the size difference between cloned and non cloned
methods. To this end, we removed all such methods along with their clones and conducted the experiments explained in Section~\ref{sec:nos1}. We observe no change in the conclusions for the \textit{Complexity}, \textit{Modularity}, and \textit{Documentation} metrics of cloned and non-cloned methods. Also, the size difference between the cloned and non-cloned methods still persists -- cloned methods, on an average, are 18\% smaller than the non-cloned methods with respect to the \textit{NOS} metric. 

\subsubsection{Comprehensibility} 

We capture the comprehensibility of a method using a Likert Scale
ranging from 1 to 5, where a score of 1 means that the method is very
easy to understand and 5 indicates that the method is very
difficult to understand. This is an entirely subjective judgment,
which is based on many variables that are not controlled for, including
our own experience. Nevertheless, we believe this exercise was worth
doing, because it would allow us to detect patterns, if they were to
exist. For example, would the cloned methods appear to be simpler than
the non-cloned methods?

Comprehensibility here is loosely related to the complexity metrics of
the previous section, but it's not the same. Code may be hard to
understand even when the logic is very simple -- e.g. when the
variable names are incomprehensible or the reader is not familiar with
the used APIs.

We note that the judges were not given any rubric to score the methods. They were allowed to create their own rubric matrix. The judges, independently found following criteria to score the methods: i) the methods with hard to understand identifiers (variables, types) are mostly harder to comprehend, ii) the methods with many conditional paths and nested conditions are harder to follow, iii) the methods which are bigger in size are usually harder to comprehend, as judges need more time and focus to comprehend such methods.

\begin{table}
\begin{center}
\begin{tabular}{l*{6}{c}r}
Type      & VE & E & M & H & VH \\ \hline
Clone     & 31 & 65 & 28 & 13 & 13 \\
Non-Clone & 32 & 69 & 32 & 10 &  7\\
\end{tabular}
\caption{Comprehensibility of cloned and non-cloned methods}
\label{tab:comprehensibility}
\end{center}
\end{table}

Table~\ref{tab:comprehensibility} shows the results. Column
\textit{Type} contains the type of the methods analyzed; column
\textit{VE}, which corresponds to option 1 in the Likert scale, shows
the number of the methods that were very easy to understand; column
\textit{E}, (option 2) shows the number of methods that were easy to
understand; column \textit{M} (option 3) contains the number of
methods that were neither easy nor hard to understand; column
\textit{H} (option 4) shows the number of methods that were hard to
understand; and column \textit{VH} (option 5) shows the number of
methods that were very hard to understand.

The distributions of both types of methods are similar, indicating
that there were no clear differences between cloned and non-cloned
methods in terms of their comprehensibility. We note, however, that we
classified more cloned methods in the VH category (13) than non-cloned
methods (7) in that same category.

To test whether the distribution of cloned and non-cloned methods across the comprehensibility categories are statistically different, we conducted a Chi-Square test. The test result ($\chi^2=2.5932, p=0.628, df=4$) shows that the distributions are not statistically different.

Further, we wanted to see whether there is any difference between the distribution of cloned and non-cloned methods if we categorize the methods into two categories: i) methods which are very easy or easy to comprehend and ii) methods which are hard or very hard to comprehend. To this end, we performed another Chi-Square test after combining \textit{VE} and \textit{E} into one category, and \textit{H} and \textit{VH} into another category. Again, we did not observe any statistically significant difference ($\chi^2=1.5033, p=0.220, df=1$).
		
Listing~\ref{lst:veasymethod} shows a small sized method which was
classified as very easy to understand. The method uses familiar
variable names and performs a task of calling \textit{appendField}
method. The method shown in Listing~\ref{lst:vhardmethod} is an
example of a method classified as very hard to understand. The identifiers (method-names, variable-names, type-names) used in the method do not help much in understanding their purpose. However, some identifiers like \textit{Token}, \textit{jj\_scan\_token}, \textit{LBRACE}, and \textit{RBRACE} do suggest that the method is probably part of some tokenization or parsing process. 

	
	\begin{minipage}{0.95\textwidth}
	\begin{lstlisting}[language=Java, label={lst:veasymethod},caption={Example of a very easy to understand method}
	]
	public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
	  {
	    List<Format> theFormat;
	    theFormat = this.getFormat();
	    strategy.appendField(locator, this, "format", buffer, theFormat);
	  }
	  {
	    List<DCPType> theDCPType;
	    theDCPType = this.getDCPType();
	    strategy.appendField(locator, this, "dcpType", buffer, theDCPType);
	  }
	  return buffer;
	}
	\end{lstlisting}
	
	\begin{lstlisting}[language=Java, label={lst:vhardmethod},caption={Example of a very hard to understand method}
	]
    private boolean jj_3R_92() {
        if (jj_scan_token(LBRACE)) return true;
        Token xsp;
        while (true) {
            xsp = jj_scanpos;
            if (jj_3R_121()) { jj_scanpos = xsp; break; }
        }
        if (jj_scan_token(RBRACE)) return true;
        return false;
    }
	\end{lstlisting}
	\end{minipage}
	
Not surprisingly, we observed some positive correlation between the size
of the methods and their comprehensibility. Most of the methods that
were hard to understand were also bigger in size. However, we found
some methods that were small and yet hard to understand, such as the
one shown in Listing~\ref{lst:vhardmethod}.
	
\subsubsection{Usability of Comments}
\label{sec:commentsqual}
We wanted to understand whether the provided comments are helpful in
the comprehensibility of methods, and whether there were
differences in the comments of cloned vs. non-cloned methods. As such, we captured the
subjective usefulness of the comments using a Likert Scale between 1
and 4, where the usability of comments increases as we move from
option 1 to option 4. This is related to the comments metrics of the
quantitative analysis, but it's not the same. Specifically, the
quantitative metrics cannot capture whether a comment is useful or
not; only people can do that.

\begin{table}
\begin{center}
\begin{tabular}{l*{5}{c}r}
Type      & NC & JC & UC & VUC \\ \hline
Clone     & 90 & 7 & 39 & 14  \\
Non-Clone & 76 & 3 & 49 & 22 \\
\end{tabular}
\caption{Usability of comments on cloned and non-cloned methods}
\label{tab:comments}
\end{center}
\end{table}
 
Columns 2 to 6 in Table~\ref{tab:comments}
correspond to each of these options. Column \textit{Type} contains the
type of the methods analyzed; column \textit{NC}, which corresponds to
option 1 in the Likert scale, shows the number of methods which had no
comments; column \textit{JC}, (option 2) shows the number of methods
which have mostly junk comments or commented out code or comments that
were practically useless; column \textit{UC} (option 3) contains the
number of methods which have somewhat useful comments. These comments
do help in the code comprehension or to understand the purpose of
given methods; column \textit{VUC} (option 4) shows the number of
methods which have very useful comments. Again, judges were not given any rubric to score the methods. They used their own intuitions to score. We note that in the cases where the judges found a mix of junk and useful comments, priority was given to the useful comments.
	
The difference between the quality of comments is noticeable between
cloned and non-cloned methods. There are 90 methods for which clones
have no comments, whereas there are only 76 non-cloned methods with no
comments. Moreover, the number of somewhat useful and extremely
useful comments for non-cloned method are also larger than for the
cloned methods. 

To test whether the distribution of cloned and non-cloned methods across the usability of comments categories are statistically different, we conducted a Chi-Square test. The test result ($\chi^2=5.6949, p=0.13, df=3$) shows that the distributions are not statistically different.

Further, we wanted to see whether there is any difference between the distribution of cloned and non-cloned methods if we categorize the methods into two categories: i) methods with no or junk comments and ii) methods with useful or very useful comments. To this end, we performed another Chi-Square test after combining \textit{NC} and \textit{JC} into one category, and \textit{UC} and \textit{VUC} into another category. Again, we did not observe any statistically significant difference ($\chi^2=3.9727, p=0.046, df=1$).

When we looked into whether there is any correlation
between the quality of comments and the size of the methods, we found
no statistically significant correlation.
	
Listing~\ref{lst:EUCOM} shows an example of very useful comments. The comments in this method not only help to understand what the code is supposed to do, but also help to understand the rationale behind the decisions made in the code.

    \begin{minipage}{0.95\textwidth}
    \begin{lstlisting}[language=Java, label={lst:EUCOM},caption={A method with extremely useful comments}
	]
	/**
	* Execute the Mojo.
	* <p>
	* The metadata model is loaded, and the specified template is executed with
	* any template output being written to the specified output file.
	* </p>
	*/
	public void execute() throws MojoExecutionException
	{
	  try
	  {
	    if (modelIds == null)
	    {
	      modelIds = new ArrayList();
	      modelIds.add(project.getArtifactId());
	    }
	    // Load the metadata file from an xml file (presumably generated
	    // by an earlier execution of the build-metadata goal.
	    Model model = IOUtils.loadModel(new File(buildDirectory, metadataFile));
	    // Flatten the model so that the template can access every property
	    // of each model item directly, even when the property is actually
	    // defined on an ancestor class or interface.
	    new Flattener(model).flatten();
	    generateConfigFromVelocity(model);
	  }
	  catch (IOException e)
	  {
	    throw new MojoExecutionException("Error during config generation", e);
	  }
	  catch (BuildException e)
	  {
	    throw new MojoExecutionException("Error during config generation", e);
	  }
	}
	\end{lstlisting} 
	\end{minipage}
	
Listing~\ref{lst:JUNK_COM} shows a method with \textit{Javadoc} comments. These comments are practically useless for a developer who wishes to understand the code. 

	\begin{minipage}{0.95\textwidth}
	\begin{lstlisting}[language=Java, label={lst:JUNK_COM},caption={A method with junk comments}
	]
    /**
    * @param jsonValues
    * @return
    * @throws JSONException
    */
    private static long[] convertJSONToLongArray(JSONArray jsonArray) throws JSONException
    {
      long[] longArray = new long[jsonArray.length()];
      if (jsonArray != null && jsonArray.length() > 0)
      {
        for (int i = 0; i < jsonArray.length(); i++)
        {
          longArray[i] = jsonArray.getLong(i);
        }
      }
      return longArray;
    }
	\end{lstlisting} 
	\end{minipage}
\subsubsection{Functionality} 

We wanted to understand whether cloned and non-cloned methods were
doing fundamentally different things. As such, we analyzed these
methods in terms of the functionality they seem to be implementing.

\textbf{Coding.} To label functionality with the methods, we use a standard method named \textit{Coding}.
Miles and Huberman note: \textit{Codes are tags or labels for assigning units of meaning to the descriptive or inferential
information compiled during a study. Codes are usually attached to ‘chunks’ of varying size –
words, phrases, sentences or whole paragraphs}~\cite{miles1994qualitative}.

\textbf{Coding Methodology.} After a first pass, we tagged each method with our best guess of the
functionality that they seem to implement. This exercise took around 5
hours per person to tag 100 methods each. After the tagging was done,
we went through all the tags to create a set of normalized tags. To
understand the normalization process, consider an example, a list
containing the following non-normalized tags: \textit{“Sorting”, “Sort”,
  “Parsing”, “Parser”, “Parse”}.  To normalize this list, we replaced
semantically similar tags with a common tag. After the normalization
of tags, the methods were re-tagged with the normalized tags by
replacing the original tag with their normalized counterpart.

We note that this process of tagging methods had some challenges. In
many cases it was difficult to come up with an exclusive tag that fully
captured the functionality of the method because of several factors --
size of methods (bigger methods usually tend to do more than one
thing, making it hard to associate them with one functionality);
complex application specific logic; lack of context; among
others. Also, there is no fixed set of functionalities that we could
have used in tagging. Nevertheless, we believe this exercise was
important, as it captured some of the essence of what we observed in
the methods.  The list of normalized tags along with their description
is as follows.

\begin{itemize}
	
	\item \textit{AppLogic}, a method that usually performs multiple responsibilities, often manifested in the form of two or more method calls.
	
	\item \textit{Wrapper}, a method that often validates certain pre-conditions, transforms the parameters (e.g. objects) to create parameters for another method call with a similar responsibility as the caller.
	
	\item \textit{Iterator} a method that iterates over a collection and executes one or more statements in a loop.
	
	\item \textit{Conditions}, a method that uses a series of if else or switch case conditions to do the main task.
	
	\item \textit{Complex Setter}, a method that sets a value of a class attribute. It includes setter with some validation code, methods which sets values of multiple fields, and also setters which set only one field. Listing~\ref{lst:csetter} shows an example clone pair, where we tagged these methods with \textit{Complex Setter} tag. The methods set the value of a variable (\textit{defaultPrefix} in the top, and \textit{localPrefix} in the bottom snippet) to the one passed to their arguments. But before setting the variable, they obtain a write-lock, perform some sanity checks, and also raise an exception if the argument received is an illegal argument.
	
	\item \textit{Complex Getter}, a method that return a value of a class attribute or an object like a \textit{getInstance} method found in singleton classes. It includes getters with some validation code, methods which execute a one or more boolean expressions, and also getters which get only one field. Listing~\ref{lst:cgetter} shows an example clone pair, where we tagged these methods with \textit{Complex Getter} tag. The methods returns the requested object if the object is not null, else it creates an instance and returns it.
	
	\item \textit{Parser}, a method that transforms a piece of
          code (e.g. String) based on grammar rules into another form
          (e.g. AST). Lexers are also included in this tag.
	
	\item \textit{StringBuilder}, a method that builds a string or a byte array, usually by calling append many times. The methods tagged as StringBuilders need not use the \textit{StringBuilder} and \textit{StringBuffer} APIs. We used the term \textit{StringBuilder} to broadly classify methods which reflect a similar behavior. Listing~\ref{lst:stringbuilder} shows an example clone pair, where we tagged these methods with \textit{StringBuilder} tag. These methods, though, do not use Java's StringBuilder API, but their implementation is very similar to how one would use a \textit{StringBuilder} API. They make multiple calls to the \textit{append} method, appending characters to the \textit{dest} object.
	
	\item \textit{Math}, a method that takes numbers as input and performs a mathematical operation with them.
	
	\item \textit{Pattern Matching}, a method that matches a pattern in a string.
	
	\item \textit{Serializer}, a method that transforms an Object
          into string or bytes, or vice-versa.
	
	\item \textit{Clone}, a method that clones another object.
	
	\item \textit{Equals}, a method that overrides \emph{equals} method.
	
	\item \textit{HashCode}, a method that overrides \emph{HashCode} method.
	
	\item \textit{Testcase}, a method representing a unit test case to test a scenario.
		
	\item \textit{Logger}, a method whose main responsibility is to log execution details.
	
	\item \textit{FileIO}, a method whose main responsibility is to write/read to/from a file.
	
	\item \textit{Socket}, a method which uses sockets to perform networking tasks.
	
	\item \textit{NA}, methods which are usually overloaded with a series of very complex tasks and it is hard to classify them in any specific bucket of functionality. 
	
\end{itemize}

Figure~\ref{fig:featuretags} shows the bar charts for clone and non
cloned methods showing their distribution across functionality
tags. We observe that many cloned and non-cloned methods are tagged
with \textit{AppLogic}, \textit{Wrapper}, \textit{Iterator},
\textit{Conditions}, \textit{Getter}, \textit{Setter},
\textit{Parser}, and \textit{StringBuilders}. In most of these tags,
except for \textit{Wrapper}, \textit{Parser}, and
\textit{StringBuilder}, there are more non-cloned methods
than cloned methods. The observation indicates that although cloning
exists in most of the functionalities, there exists some
functionalities where methods have much structural similarities to be
deemed clones.

To test whether the distribution of cloned and non-cloned methods across the functionality tags are statistically different, we conducted a Chi-Square test. The test result ($\chi^2=35.119, p=0.009, df=18$) shows that the distributions are statistically different.
%(*@\hl{defaultPrefix = null;}@*)

\begin{minipage}{0.95\textwidth}
	\begin{lstlisting}[language=Java, label={lst:stringbuilder},caption={An example clone pair (Type-3), demostrating string builder like behavior.}
	]
	private static void (*@\hl{appendTextRow}@*)(final (*@\hl{ByteBuffer}@*) buffer, final Appendable dest, final int startPos, final int columns) throws IOException {
	    final int limit = buffer.limit();
	    int pos = startPos;
	    (*@\hl{dest.append('[');}@*)
	    (*@\hl{dest.append(' ');}@*)
	    for (int c = 0; c < columns; c ++) {
	        for (int i = 0; i < 8; i ++) {
	            if (pos >= limit) {
	                dest.append(' ');
	            } else {
	                final char v = (*@\hl{(char)}@*) (buffer.get(pos++) & (*@\hl{0xff);}@*)
	                (*@\hl{if (Character.isISOControl(v))}@*) {
	                    (*@\hl{dest.append('.');}@*)
	                } else {
	                    (*@\hl{dest.append(v);}@*)
	                }
	            }
	        }
	        dest.append(' ');
	    }
	    (*@\hl{dest.append(']');}@*)
	}
	\end{lstlisting}
	\begin{lstlisting}[language=Java]
	private static void (*@\hl{appendHexRow}@*)(final (*@\hl{CharBuffer}@*) buffer, final Appendable dest, final int startPos, final int columns) throws IOException {
        final int limit = buffer.limit();
        int pos = startPos;
        for (int c = 0; c < columns; c ++) {
            for (int i = 0; i < 8; i ++) {
                if (pos >= limit) {
                    dest.append(' ');
                } else {
                    final char v = buffer.get(pos++);
                    (*@\hl{final String hexVal = Integer.toString(v, 16);}@*)
                    (*@\hl{dest.append("0000".substring(hexVal.length()));}@*)
                    (*@\hl{dest.append(hexVal);}@*)
                }
                dest.append(' ');
            }
            (*@\hl{dest.append(' ');}@*)
            (*@\hl{dest.append(' ');}@*)
        }
    }
    \end{lstlisting}
\end{minipage}


\begin{minipage}{0.95\textwidth}
	\begin{lstlisting}[language=Java, label={lst:csetter},caption={An example clone pair (Type-3), demostrating complex setter behavior.}
	]
    public void (*@\hl{setDefaultPrefix}@*)(final String prefixString) {
        try {
            lock.writeLock().lock();
            if (prefixString == null || StringUtils.isBlank(prefixString)) {
                (*@\hl{defaultPrefix = null;}@*)
            } else {
                if (prefixes != null) {
                    if (!prefixes.contains(prefixString)) {
                        throw new IllegalArgumentException("The given prefix is not part of the prefixes.");
                    }
                }
                (*@\hl{defaultPrefix = prefixString;}@*)
            }
        } finally {
            lock.writeLock().unlock();
        }
    }
	\end{lstlisting}
	\begin{lstlisting}[language=Java]
    public void (*@\hl{setPrefix}@*)(final String prefixString) {
        try {
            lock.writeLock().lock();
            if (prefixString == null || StringUtils.isBlank(prefixString)) {
                (*@\hl{localPrefix.remove();}@*)
            } else {
                if (prefixes != null) {
                    if (!prefixes.contains(prefixString)) {
                        throw new IllegalArgumentException("The given prefix is not part of the prefixes.");
                    }
                }
                (*@\hl{localPrefix.set(prefixString);}@*)
            }
        } finally {
            lock.writeLock().unlock();
        }
    }
	\end{lstlisting}
\end{minipage}

\begin{minipage}{0.95\textwidth}
	\begin{lstlisting}[language=Java, label={lst:cgetter},caption={An example clone pair (Type-2), demostrating complex getter behavior.}
	]
	public HTML (*@\hl{getOptionsCaption()}@*) {
	    if (optionsCaption == null) {
	        optionsCaption = new HTML();
	        optionsCaption.setStyleName(CLASSNAME + "-caption-(*@\hl{left} @*)");
	        optionsCaption.getElement().getStyle()
	        .setFloat(com.google.gwt.dom.client.Style.Float.(*@\hl{LEFT} @*));
	        captionWrapper.add((*@\hl{optionsCaption} @*));
	    }
	    return (*@\hl{optionsCaption} @*);
    }
	\end{lstlisting}
	\begin{lstlisting}[language=Java]
    public HTML (*@\hl{getSelectionsCaption()} @*) {
        if (selectionsCaption == null) {
            selectionsCaption = new HTML();
            selectionsCaption.setStyleName(CLASSNAME + "-caption-(*@\hl{right} @*)");
            selectionsCaption.getElement().getStyle()
            .setFloat(com.google.gwt.dom.client.Style.Float.(*@\hl{RIGHT} @*));
            captionWrapper.add((*@\hl{selectionsCaption} @*));
        }
        return (*@\hl{selectionsCaption}@*);
    }
	\end{lstlisting}
\end{minipage}

\begin{figure}
	\centering
	\includegraphics[width=13cm]{plots/qualitative/functionality.pdf}
	\caption {\scriptsize Distribution of cloned and non-cloned
          methods across functionality tags. The Vertical-axis shows the functionality tags and the Horizontal-axis shows the number of methods in each functionality tag.}
	\label{fig:featuretags}
	\vspace{-5mm}
\end{figure}

\input{deepanalysis}