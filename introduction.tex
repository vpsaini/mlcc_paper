\section{Introduction}
\label{sec:introduction}
		% Computer Society journal (but not conference!) papers do something unusual
		% with the very first section heading (almost always called "Introduction").
		% They place it ABOVE the main text! IEEEtran.cls does not automatically do
		% this for you, but you can achieve this effect with the provided
		% \IEEEraisesectionheading{} command. Note the need to keep any \label that
		% is to refer to the section immediately after \section in the above as
		% \IEEEraisesectionheading puts \section within a raised box.
		
Studies have shown that there is a fair amount of code duplication in
software~\cite{lopes:oopsla17,antoniol2002analyzing}, and a variety of reasons
have been given to explain this phenomenon~\cite{johnson:94,baker:1995wcre,Baxter:1998rt}. Independently
of whether cloning is justified or simply a bad practice, the
identification of similar pieces of code is very important, both in
practice (e.g. for refactoring and improving software quality) and for
research (e.g. for avoiding biased datasets).  

Clone detection is the process of locating exact or similar pieces of
code fragments within, or between, software systems. Over the past 20
years, clone detection has been the focus of increasing attention,
with many clone detectors having been proposed and implemented
(see~\cite{Sheneamer:survey16} for a recent survey on this topic). For
the purpose of contextualizing our work, we briefly summarize all that
work here.

There are many kinds of clone detection approaches and tools,
depending on the goals and granularity of the detection. In the early
days of clone detection, file name matching was frequently used as an
heuristic for finding file-level clones (e.g. \cite{Mockus:2007}),
and, to this day, it is still a valid and simple heuristic for certain
clone detection goals. Most clone detectors, however, look at the
contents of the files, either because they aim at finding
finer-granularity code duplication or because they aim at improving
the detection process (or both). Within these, there are four broad
categories of clone detection approaches, ranging from easy-to-detect
to hard-to-detect clones: textual similarity, lexical similarity,
syntactic similarity, and semantic similarity. The literature refers
to them by the four commonly accepted types of
clones~\cite{bellon,roy:queens:07}:

\begin{itemize}
\item Type-1 (textual similarity): Identical code fragments, except
  for differences in white-space, layout and comments.
\item Type-2 (lexical, or token-based, similarity): Identical code
  fragments, except for differences in identifier names and literal
  values.
\item Type-3 (syntactic similarity): Syntactically similar code
  fragments that differ at the statement level. The fragments have
  statements added, modified and/or removed with respect to each
  other.
\item Type-4 (semantic similarity): Code fragments that are
  semantically similar in terms of what they do, but possibly
  different in how they do it. This type of clones may have little or
  no lexical or syntactic similarity between them. An extreme example
  of exact semantic similarity that has almost no syntactic
  similarity, is that of two sort functions, one implementing bubble
  sort and the other implementing selection sort.
\end{itemize} 

The various types of clones are defined in terms of similarity
metrics, which means that clone detectors may be looking for exact
matches (100\% similarity on the metric of choice) or relaxed matches
that pass a certain threshold of similarity. In any case, an ideal
clone detector would be able to detect all of these types of clones,
even if specific usages would focus on only some of them at a time. In
practice, clone detectors use a variety of signals from the code
(text, tokens, syntactic features, program dependency graphs, etc.) and
tend to aim for detecting specific types of clones, usually up to
Type-3. Very few of them attempt detecting Type-4 clones. 

One aproach that tends to have very high recall in detecting up to
Type-3 clones is the use of software metrics, such as number of
conditionals, number of statements, number of arguments, etc. Given
that only these metrics are looked at, the metrics-based approach is
resilient to changes in identifiers and literals, which is at the core
of Type-3 cloning. However, the metrics-based approach, by itself,
tends to have very poor precision, as it falsely identifies a large
number of fragments as clones of each other when they happen to have
similar metrics. For that reason, clone detectors that use metrics use
additional features to prevent so many false positives. Another
problem with using metrics for anything other than 100\% similarity is
fine-tuning the thresholds of similarity: given that many metrics are
used, there is a large number of configurations between the
thresholds of each individual metric, and finding the right balance
can be hard (e.g. is the number of conditionals more meaningful than
the number of arguments?).

Because clone detection can be processing-intensive, clone detectors may
suffer from scalability problems, limiting the size of the dataset on
which they operate. For large datasets, the runtime efficiency of
clone detectors is as important as their effectiveness in detecting
clones. Hence, features that are easier to extract are
preferred to those that require longer processing. Similarly,
identifying and eliminating pairs that aren't clones early on in the
processing pipeline is much better than propagating those pairs until
the end.

In this paper, we present \Name, a scalable clone detector that is
capable of detecting not just Type-1 through Type-3 clones, but also
semantically similar fragments (Type-4) with substantially different
tokens and syntax. The key insights behind the development of
\Name\ are twofold: (1) functionally similar pieces of code tend to do
similar {\em actions}, as embodied in the functions they call and the
state they access; and (2) not all pieces of code that do similar
actions are functionally similar; however, it is possible to {\em
  learn}, by examples, a combination of metric weights that can
predict whether two pieces of code that do the same actions are clones
of each other. Figure~\ref{fig:overview} illustrates the overview of
the approach. For semantic similarity, we use a novel \textit{Action
  Filter} to filter out a large number of method pairs that don't seem
to be doing the same actions, focusing only on the candidates that
do. For those potential clones, we pass them through a supervised
machine-learned metrics-based model that predicts whether they are
clones or not; this cognitive component shares some similarities with
the work presented in~\cite{Sheneamer:icmla16}. We demonstrate that
\Name\ is highly accurate and at the same time, scalable.

\begin{figure}
	\centering
	\fbox{\includegraphics[scale=0.6] {Process_outline.pdf}}
	\caption{Overview of the approach.}
	\label{fig:overview}
	\vspace{-0.225in}
\end{figure}

The results presented in this paper were obtained by training the metrics
similarity models using SourcererCC~\cite{sajnani2016sourcerercc}, a
state of the art clone detector that has been shown to have fairly
good precision and recall up to Type-3 clones (but not
Type-4). However, our approach is not tied to SourcererCC; any
accurate clone detector can be used to train the models. Specifically,
many clone detection techniques like graph-based or AST-based, which
are accurate but hard to scale, can be used for the training
phase. More generally, \Name\ is designed to incorporate models from an
ensemble of accurate clone detectors that specialize in certain kinds
of clones, in order for the accuracy to be even better.
		
In our experiments, the recall values for \Name\ are similar to
SourcererCC in detecting Type-1 and Type-2 clones. However,
\Name\ performs much better on Type-3 and even better on Type-4
clones, which neither SourcererCC nor the vast majority of clone
detectors are designed to detect. The number of Type-4 clones detected
by \Name\ is two orders of magnitude higher than SourcererCC. This is
very encouraging, as it shows that a relatively simple semantic
heuristic used {\em before} a metrics-based clone detector component can
accurately identify hard-to-detect Type-4 clones without hurting the
accuracy of detection of all other types of clones.


%% Moreover, the metrics based approaches are good
%% candidates to detect clones incrementally. With every new input
%% dataset one needs to calculate the metrics for code fragments in this
%% newer dataset and then compare these newer methods to the methods in
%% older and this new dataset. There is no need to reprocess the older
%% dataset.
		

%% There are however a few issues with such approaches: i) metrics-based
%% approaches are prone to false-positives. This happens because metrics
%% comparison ignores the semantics of the code fragments, which leads to
%% the detection of false positives; ii) the rules and thresholds to
%% compare candidate-pairs are defined manually. With increase in the
%% number of metrics, it becomes increasingly difficult to capture the
%% optimum rules and thresholds. iii) candidate-explosion. The number of
%% candidates, for which metrics should be compared before labeling them
%% as clones or non-clones, get very big with the increase in the size of
%% the dataset. A naive approach would compare each method to every other
%% method. A slightly better approach would filter out candidates based
%% on some size-threshold. None of these approaches scale very well to
%% large datasets though; and iv) the candidate-explosion issue leads to
%% the increase in the total runtime of the system, and so metrics-based
%% approaches tend to be slow.
		
%% There are other techniques like hybrid of token and index-based
%% techniques which are shown to be fast, scalable, and highly
%% parallel-able~\cite{}. These techniques need some source-code
%% normalization techniques to be able to detect harder Type-3
%% clones~\cite{}. The detection gets harder when most of the tokens in
%% two methods are different. This is because to validate a clone pair,
%% many tokens in one method need to be found in the candidate clone
%% method.
		
%% Also, researchers have tried combination of metrics and machine
%% learning based techniques to train models to learn the complex rules
%% and thresholds~\cite{}. These techniques are promising but they also
%% suffer from the candidate explosion issue and therefore are hard to
%% scale.
		
%% To take advantage of the desirable qualities of the metric-based,
%% information-retrieval-based and machine learning based approaches we
%% decided to address the above mentioned issues. To this end, we present
%% a novel approach, which is a combination of information-retrieval,
%% metrics, and machine-learning based techniques.
		
		
%		\textbf{Need for Incremental, scalable and accurate clone detection.}
%		
%		\textbf{Existing techniques which are accurate but do not scale}
%		
%		\textbf{Existing techniques which are accurate, shown to scale but are not incremental. Also, the type of clones they will miss.}
%		
%		\textbf{To address the need and inherit the qualities of accurate clone detectors we present our approach which is fast, scalable, incremental and highly parallel-able. }
		
	
		%We note that we are only looking at the quantitative aspect of \textit{Documentation} and are not analyzing the documentation qualitatively.
		
		% Talk about the results here

\noindent{\bf Contributions:}
The most important contribution of this work is the scalable approach
used to detecting up to Type-4 clones. Even though \Name\ is not the
first clone detector capable of tackling the difficult niche of Type-4
clones using a metrics-based component, the approach is scalable to
very large datasets. We focus on scalability, because that is
important to projects that involve very large source code datasets: a
clone detector that doesn't scale well with the size of input is
not very useful in practice. With that in mind, below we list the concrete
contributions of this paper:

\begin{itemize}
					
\item \textbf{Action filter}. We introduce this novel filter which not
  only captures the semantics of the methods but also filters out many
  candidate pairs which are unlikely to be clones.
\item \textbf{Process-pipeline for scalable, incremental, and accurate
  metric-based and learning-based clone detection}. Metrics based
  techniques usually suffer from high false positive rate. Moreover,
  they suffer from candidates-explosion problem which limits them to
  smaller datasets. We demonstrate in this paper how one can achieve
  scalable, fast clone detection using metrics and machine learning.
\item \textbf{Process-pipeline to learn from slow but accurate clone
  detector tools and scale to large datasets}. Many clone detection
  techniques like graph-based, AST-based which are accurate but hard
  to scale can be used to train models. The trained model then can be
  used to predict clones on larger datasets.
\item \textbf{Analysis of Type-4 clones}. Additionally to
  quantitative results, we present analysis of examples of
  Type-4 clones, the difficult task, even for humans, of deciding
  whether they are clones, and the reasons why \Name\ succeeds
  where other clone detectors fail.
					 										
\end{itemize}
		
		
\noindent \textbf{Outline:} The remainder of this paper is organized
as follows: Section \ref{sec:feasibility} describes the process we
followed to perform a feasibility study of our idea; Section
\ref{sec:concepts} presents three concepts that are parts of our
proposed approach and are critical to its performance; Section
\ref{approach} explains the main process of the this approach; Section
\ref{sec:evaluation} elaborates on the evaluation of approach; We present the manual analysis of clone pairs in Section~\ref{sec:qualitative}; Section
\ref{sec:related} discusses related works in this area; Section
\ref{sec:limitations} presents the limitations of this study, and
finally, Section \ref{sec:conclusion} discusses conclusions and future
works.
