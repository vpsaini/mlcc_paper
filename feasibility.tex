\section{Reproduction Study} 
\label{sec:feasibility}

In~\cite{Sheneamer:icmla16}, Sheneamer and Kalita describe a
metrics-based supervised machine learning approach to detect Type-4
clones. We started the design of \Name\ by reproducing a similar
version of that work in order to verify whether, indeed, it was
fruitful to train a model on metrics for purposes of clone
detection. The results of our reproduction study were
encouraging. Most importantly, this study provided us with valuable
lessons as to the strengths and pitfalls of this approach, which were
critical to the development of \Name.

In this reproduction study, a simple process for Feature Generation,
Model Train, and Prediction on a sample dataset was carried out.  The
dataset used in this study is explained in
Section~\ref{sec:feas_dataset}, the experiments conducted through this
study are provided in Section \ref{sec:feas_experiments}, and finally,
the lessons learned are described in Section \ref{sec:feas_lessons}.

\subsection{Dataset}
\label{sec:feas_dataset}

For the purpose of this study, we used the dataset of 3,562 Java projects made available by Saini et
al~\cite{saini2016comparing}. The
dataset consists of 3,562 Java projects hosted on
Maven~\cite{Maven}. The comprehensive list of projects with their
version information is available
at~\url{http://mondego.ics.uci.edu/projects/clone-metrics/}.

\textbf{Dataset Artifacts and Properties:}
We used following artifacts from the dataset described above: 

 \textit{1) Clone-pairs}. Intra-project method-level clone detection
 results for 3,562 projects. The clone detection was carried out using
 SourcererCC. Overall, 412,705 cloned and 616,604 non-cloned methods were identified by SourcererCC in this dataset.

 \textit{2) Method-level metrics}. This artifact consists of
 method-level metrics calculated for the methods in this dataset. The metrics were calculated at the using
 version 6 of the JHawk tool ~\cite{urlJHawk}. JHawk has been widely
 used in academic studies on Java metrics
 ~\cite{gupta2005fuzzy,alghamdi2005oometer,benestad2006assessing,arisholm2006predicting,arisholm2007data}. Table~\ref{tab:metric-breakdown}
 shows the $25$ metrics for which the computed values were available
 in the artifact. Many of these metrics are standard metrics. A set of
 metrics are derived from the Software Quality Observatory for Open
 Source Software (SQO-OSS)~\cite{Samoladas:2008pb}. SQO-OSS is
 composed of well-established and validated software quality metrics,
 which can be computed either from source code or from surrounding
 community data. More details on these metrics and the dataset can be found
 elsewhere~\cite{saini2016comparing}.

\label{sec:quality-metrics}
\begin{table}[!tbp]
	\begin{center}
		\resizebox{8cm}{!}{
			\begin{tabular} {l l l}
				\thickhline
				\hlx{v}
				Name & Description \\
				\hlx{vhv}
				\hlx{vhv}
				XMET & External methods called by the method\\
				VREF & Number of variables referenced\\
				VDEC & Number of variables declared\\
				TDN & Total depth of nesting\\
				NOS & Number of statements\\
				NOPR & Total number of operators\\
				NOA & Number of arguments\\
				NLOC & Number of lines of code\\
				NEXP & Number of expressions\\
				NAND & Total number of operands\\
				MOD & Number of modifiers\\
				MDN & Method, Maximum depth of nesting\\
				LOOP & Number of loops (for,while)\\
				LMET & Local methods called by the method\\
				HVOL & Halstead volume\\
				HVOC & Halstead vocabulary of method\\
				HLTH & Halstead length of method\\
				HEFF & Halstead effor to implement a method\\
				HDIF & Halstead difficulty to implement a method\\
				HBUG & Halstead prediction of number of bugs\\
				EXCT & Number of exceptions thrown by the method\\
				EXCR & Number of exceptions referenced by the method\\
				CREF & Number of classes referenced\\
				COMP & McCabes cyclomatic complexity\\
				CAST & Number of class casts\\

				\thickhline
			\end{tabular}
		}
		\caption{Software Quality Metrics}
		\label{tab:metric-breakdown}
	\end{center}
\vspace{-0.245in}
\end{table}
\subsection{Experiments} \label{sec:feas_experiments}
The experiments carried out to implement the whole process of our idea are elaborated in this section.
\subsubsection{Features Preparation} \label{sec:feas_featureprep}
In order to leverage software metrics calculated by JHawk in Machine Learning, a new dataset needed to be created with applicable features such that these features can capture similarities and differences in method pairs.   
% and is depicted in Figure \ref{fig:featureprep} \linebreak 
%\begin{figure}
%	\fbox{\includegraphics[scale=0.75] {Feature_Eng.jpg}}
%	\caption{Feature Generation Process in Feasibility Study}
%	\label{fig:featureeng}
%\end{figure}
%As illustrated by Figure \ref{fig:featureprep}, 
%In this process, first, Java source files are given to Metrics Calculator (Jhawk), and rows of \{method Id, metrics\} are formed. However, in this approach, we need to have a dataset that defines the characteristics of method pairs (not each individual method) to distinguish clone pairs from non-clone pairs. Thus, features demonstrating the relationship of methods with each other should be produced. 
To this aim, we calculated the percentage difference for each metric in a method pair. Having a clone pair with two methods named \textit{CP1} and \textit{CP2}, if metric \textit{f} of \textit{CP1} is named \textit{f1} and metric \textit{f} of \textit{CP2} is named \textit{f2}, the percentage difference between \textit{f1} and \textit{f2} is calculated by the formula in \ref{perdiff}. We name the combination of method pairs and percentage differences a \textit{Feature Vector}. 
\begin{equation}\label{perdiff}
Percentage-diff(f1,f2)=\dfrac{|f1-f2|}{Max(f1,f2)}*100
\end{equation}

%As a result of this, if  metrics for method \textit{m1} are denoted in \ref{row1} and metrics for method \textit{m2} are illustrated in \ref{row2}, then the Feature Vector  derived from these two rows in the prepared dataset will be \ref{rowmerge}. In \ref{rowmerge}, the first two columns are names for methods in the pair at hand, and the rest of columns contain the percentage difference between two methods for COMP, NOS, HLTH, HVOC, HEFF, HBUG, CREF, XMET, LMET, NLOC, NOA, MOD, HDIF, VDEC, EXCT, EXCR, CAST, TDN, HVOL, NAND, VREF, NOPR, MDN, NEXP, and LOOP metrics, respectively.
%
%\begin{equation} \label{row1}
%\begin{matrix}
%\{method1,2,8,54,29,1475.61,0.09,5,3,2,10,1,1,5.62,1,0,0,0,\\1,262.33,25,8,29,1,10,0\}
%\end{matrix}
%\end{equation}
%\begin{equation} \label{row2}
%\begin{matrix}
%\{method2,2,9,95,39,5272.19,0.17,4,8,0,12,0,1,10.5,1,0,0,0,\\2,502.11,42,13,53,1,19,0\} 
%\end{matrix}
%\end{equation}
%\begin{equation} \label{rowmerge}
%\begin{matrix}
%\{method1,method2,0.0,11.11,43.16,25.64,72.01,47.06,20.0,\\62.5,100.0,16.67,100.0,0.0,46.48,0.0,0.0,0.0,0.0,50.0,47.75,\\40.48,38.46,45.28,0.0,47.37,0.0\}
%\end{matrix}
%\end{equation}

%The Feature Generation step is a prerequisite for both \textit{Train Model}, and \textit{Prediction} processes. In Train Model process features are used to train a model, and in Prediction (Test) process, they are used to recognize clone pairs from non-clone pairs.   
\subsubsection{Train and Test Data Creation} \label{sec:feas_traintestcreate}
We separated the whole dataset at hand randomly to two parts: 60\% for \textit{train} dataset, and the rest \textit{test}. We produced features for both datasets, and for train dataset, we integrated each Feature Vector with its corresponding \textit{is\_clone} label based on SourcererCC.

To have a manageable sized dataset, we filtered out pairs for which the percentage difference in NOS is more than 30\%. Having the dataset ready, the first step to train the target clone detector model was to select a classification model which fits our needs and has a satisfactory performance both in terms of accuracy and timing. This process is explained in Section~\ref{sec:feas_modelselec}. The trained model was then tested on the reserved unseen test data; results of this process is explained in Section~\ref{sec:feas_evaluation}. 

\subsubsection{Model Selection} \label{sec:feas_modelselec}
To select a target model, we performed 10-fold cross validation, (a common form of N-fold cross validation ~\cite{refaeilzadeh2009cross}), with various algorithms. However, since our training dataset was 45GB in size, training multiple models using 10-fold cross validation(CV) was not feasible in terms of timing. Hence, we selected three different random samples from this dataset: a sample with 10 thousand rows, a sample with 50 thousand rows, and another with 100 thousand rows. Having three different samples could help us get assured that the model selection process is general and independent of characteristics of a single dataset sample. Each of the three sampled datasets were split into 70\% train and 30\% test.
%Each of the three sampled datasets were divided to two parts: a 70\% portion named training, and a 30\% named testing. 10-fold cross validation was performed on the training dataset, and then each model was trained on the whole training dataset and tested on testing dataset. The purpose was to reserve an unseen portion in each sample 
%%and compare the results of 10-fold cross validation with results on an unseen bunch of data. This 
%that could further assure us as to the generalization of our results.


 We ran our experiments with multiple classifiers: K-Nearest Neighbors (KNN) ~\cite{wu2008top}, Naive Bayes ~\cite{wu2008top} , Classification and Regression Trees (CART) ~\cite{wu2008top}, Support Vector Machine (SVM) ~\cite{wu2008top}, Logistic Regression (LR) ~\cite{dreiseitl2002logistic}, Linear Discriminant Analysis (LDA) ~\cite{izenman2008modern}. For these models, we measured the Precision and Recall (with respect to SourcererCC) , and also, the time each of them takes.
 %Performance of each classifier after being trained on 70\% training dataset and validating on the 30\% unseen testing data is given in Table \ref{test7030}. In this table, T\textsubscript{L} denotes time taken for learning a model, T\textsubscript{P} stands for time taken to predict clones, and time measurements are in seconds. Precision and Recall numbers are measured with respect to the reference clone detector (SourcererCC). Results of 10-fold cross validation are not included because of space limitations.

%\begin{table*}[t]
%	\centering
%%	\caption{Results of 10-fold Cross Validation on Sampled Rows from Train Dataset}
%%	\label{10CV}
%	\begin{center}
%		\begin{tabular}{|l|c|c|c|c|c|c|c|c|c|c|c|c|c}
%			\cline{1-13}
%			Dataset       & \multicolumn{4}{l|}{10 Thousand Rows} & \multicolumn{4}{l|}{50 Thousand Rows} & \multicolumn{4}{l|}{100 Thousand Rows} &  \\ \cline{1-13}
%			\backslashbox {Model}{Result} & Prec.   & Recall  & F1    & Time  & Prec.   & Recall  & F1    & Time  & Prec.   & Recall  & F1    & Time   &  \\ \cline{1-13}
%			KNN (K=5)     & 92\%        & 92\%    & 92\%  & 1.6   & 92\%        & 96\%    & 94\%  & 33.6  & 93\%        & 97\%    & 95\%  & 145.3  &  \\ \cline{1-13}
%			KNN (K=10)    & 91\%        & 94\%    & 93\%  & 1.5   & 92\%        & 97\%    & 94\%  & 30.3  & 93.4\%      & 97\%    & 95\%  & 135.3  &  \\ \cline{1-13}
%			Naive Bayes   & 89\%        & 93\%    & 90\%  & 0.2   & 90\%        & 92\%    & 91\%  & 1.5   & 90\%        & 92\%    & 91\%  & 2.6 s  &  \\ \cline{1-13}
%			CART          & 89\%        & 95\%    & 91\%  & 0.6   & 92\%        & 96\%    & 94\%  & 3.2   & 93\%        & 97\%    & 95\%  & 7.2 s  &  \\ \cline{1-13}
%			SVM           & 96\%        & 89\%    & 93\%  & 31.7  & 96\%        & 93\%    & 94\%  & 1632  &             &         &       &        &  \\ \cline{1-13}
%			LR            & 92\%        & 94\%    & 93\%  & 1.1   & 91\%        & 93\%    & 92\%  & 7.4   & 92\%        & 93\%    & 92\%  & 18.1   &  \\ \cline{1-13}
%			LDA           & 87\%        & 94\%    & 90\%  & 0.3   & 88\%        & 94\%    & 91\%  & 1.9   & 88\%        & 94\%    & 91\%  & 4.6    &  \\ \cline{1-13}
%			Random Forest & 93\%        & 94\%    & 94\%  & 0.5   & 94\%        & 96\%    & 95\%  & 2.7   & 95\%        & 97\%    & 96\%  & 5.7    &  \\ \cline{1-13}
%			AdaBoost     & 93\%        & 934\%   & 94\%  & 2.9   & 94\%        & 96\%    & 95\%  & 11.7  & 95\%        & 97\%    & 96\%  & 24.4   &  \\ \cline{1-13}
%		\end{tabular}
		%	\end{table}
		
		%\begin{table}[]
		%\centering
%		\caption{Results of Training Models and Validating Them on Unseen Portion of Sampled Rows from Train Dataset}
%		\label{test7030}
%		\begin{tabular}{l|ccccc|ccccc|ccccc}
%			\hline
%			Dataset       & \multicolumn{5}{l|}{10 Thousand Rows}                 & \multicolumn{5}{l|}{50 Thousand Rows} &  \multicolumn{5}{l}{100 Thousand Rows}    \\ \hline
%			\backslashbox {Model}{Result} & Prec. & Recall & F1   & T\textsubscript{L} & T\textsubscript{P} & Prec. & Recall & F1   & T\textsubscript{L} & T\textsubscript{P} & Prec. & Recall & F1   & T\textsubscript{L} & T\textsubscript{P} \\ \hline
%			KNN     & 90\%      & 92\%   & 91\% & 0.03       & 0.7          & 93\%      & 97\%   & 95\% & 0.2       & 21           & 94\%      & 97\%   & 95\% & 0.7       & 68.7         \\ \hline
%			%KNN (K=10)    & 91\%      & 94\%   & 93\% & 0.02       & 0.7          & 93\%      & 98\%   & 95\% & 0.2       & 17           & 94\%      & 97\%   & 96\% & 0.8       & 58.4         \\ \hline
%			Naive Bayes   & 86\%      & 92\%   & 89\% & 0.02       & 0.01         & 92\%      & 92\%   & 92\% & 0.1       & 0.05         & 90\%      & 91\%   & 91\% & 0.2       & 0.08         \\ \hline
%			CART          & 88\%      & 95\%   & 91\% & 0.1        & 0.008        & 92\%      & 97\%   & 94\% & 0.3       & 0.03         & 93\%      & 96\%   & 95\% & 0.7       & 0.05         \\ \hline
%			SVM           & 93\%      & 89\%   & 91\% & 3.6        & 1.4          & 97\%      & 93\%   & 95\% & 211       & 35.4         & 96\%      & 94\%   & 95\% & 2079      & 143          \\ \hline
%			LR            & 89\%      & 93\%   & 91\% & 0.1        & 0.005        & 92\%      & 93\%   & 93\% & 0.7       & 0.03         & 91\%      & 92\%   & 92\% & 1.6       & 0.05         \\ \hline
%			LDA           & 87\%      & 94\%   & 90\% & 0.03       & 0.01         & 89\%      & 94\%   & 92\% & 0.2       & 0.04         & 87\%      & 93\%   & 90\% & 0.3       & 0.06         \\ \hline
%			Random Forest & 91\%      & 93\%   & 92\% & 0.04       & 0.01         & 94\%      & 96\%   & 95\% & 0.2       & 0.03         & 95\%      & 96\%   & 95\% & 0.5       & 0.09         \\ \hline
%%			AdaBoost     & 89\%      & 94\%   & 91\% & 0.2        & 0.01         & 95\%      & 96\%   & 95\% & 0.9       & 0.04         & 94\%      & 96\%   & 95\% & 2.9       & 0.09         \\ \hline
%		\end{tabular}
%	\end{center}\mbox{}
%\end{table*}

%At the first step, we tried single classifiers, and noted down their performance.
Based on the experiments we selected CART as it had better accuracy and speed than others.
%Experiments revealed that KNN, CART and SVM, had a better accuracy than others;  but, KNN and SVM were much more slower than CART. Thus, we selected CART as our target classifier. 
However, classification models are prone to get biased and over fitted to train data and not to generalize to unseen data well. As a result, we tested 
%an ensemble of classifiers based on the selected algorithm (CART): We tested %AdaBoost ~\cite{wu2008top} and 
Random Forest~\cite{ho1995random}, 
%both of these algorithms build an ensemble of classifiers: 
which builds several Decision Trees and takes the majority vote among them as the final vote. 
%and AdaBoost builds several models of a configured base model, and combines them ~\cite{wu2008top}. 
As it was expected, Random Forest improved accuracy, while taking more time than CART. However, the increase in time was negligible. Hence, Random Forest was selected as the final model. 
%The next step was to select an optimal configuration for Random Forest. To this end, we decided to run experiments on our sampled datasets and then proceed with a select set of configurations to the whole train dataset. After doing 10-fold cross validation, and training on 70\% of data and testing on 30\% of it, using multiple parameters on the sampled datasets, we found 5 configurations that were performing better than others. Results for these shortlisted configurations on the 100 thousand rows dataset are given in Table \ref{shortlist100k}. \linebreak
%Having shortlisted 5 classifiers, we selected X configuration as the best one and proceeded to train this model on the whole train dataset and predict using it on the unseen 40\% reserved portion of data. 

%
%\begin{table*}[t]
%	\centering
%	\caption{Performance of Random Forest Classifier on 100 Thousand Rows Sample}
%	\label{short}
%	\begin{center}
%		\begin{tabular}{|c|c|c|c|c|c|c|c|c|c|c|c|c|l|}
%			\hline
%			\multirow{2}{*}{\#Est.} & \multirow{2}{*}{Max D} & \multirow{2}{*}{Min S} & \multirow{2}{*}{Min L} & \multirow{2}{*}{Max F} & \multicolumn{4}{c|}{10- Fold Cross Validation} & \multicolumn{5}{c|}{30\% Unseen Data (2-Fold Cross Validation)} \\ \cline{6-14} 
%			&                        &                        &                        &                        & Prec.      & Recall      & F1       & Time     & Prec.      & Recall      & F1        & t          & t           \\ \hline
%			5                       & 20                     & 10                     & 5                      & Sqrt(\#Features)       & 95\%       & 96\%        & 95\%     & 5.6      & 94\%       & 96\%        & 95\%      & 0.43 s     & 0.06 s      \\ \hline
%			5                       & --                     & --                     & --                     & --                     & 95\%       & 97\%        & 96\%     & 5.7      & 95\%       & 96\%        & 95\%      & 0.49 s     & 0.09 s      \\ \hline
%			10                      & 10                     & 20                     & 5                      & Sqrt(\#Features)       & 94\%       & 95\%        & 95\%     & 8.1      & 95\%       & 95\%        & 95\%      & 0.82s      & 0.084 s     \\ \hline
%			50                      & 5                      & --                     & --                     & Sqrt(\#Features)       & 95\%       & 86\%        & 90\%     & 26.1     & 95\%       & 85\%        & 90\%      & 2.81 s     & 0.18 s      \\ \hline
%			25                      & 10                     & --                     & --                     & Sqrt(\#Features)       & 95\%       & 96\%        & 96\%     & 17.5     & 95\%       & 95\%        & 95\%      & 1.83 s     & 0.126 s     \\ \hline
%		\end{tabular}
%	\end{center}\mbox{}
%\end{table*}

\subsubsection{Evaluation} \label{sec:feas_evaluation}
To measure the performance of the selected model on unseen data, we decided to  train a model on the whole train dataset. 
%However, the whole
%training dataset had 173,766,592 rows, and training on all of these
%rows were not feasible. 
However, after analyzing the this dataset, we observed that there exists approximately 7 times more cloned pairs than non-cloned pairs. Hence, we did an inclusive sampling over the data in which equal number of clones and non-clones were present. This ensured us that the model is being trained correctly and accurately.
%observed that there were 22,151,199 clone pairs, and 151,615,393
%non-clone pairs present in the train dataset. We decided to have an
%inclusive and manageable sampling over the train dataset: A sample in
%which beside including all clone pairs, 22,151,199 random non-clone
%pairs were selected; this yielded to a dataset that included equal
%number of clones and non-clones. This dataset could also assure us
%that the model is being trained correctly and accurately as it sees
%equal number of cloned and non-cloned methods. 
Testing the trained model on the 40\%
unseen test data, resulted in 88\% Precision and 97\% Recall with respect to SourcererCC's results. Model train took 570 seconds and Prediction took 345 seconds for the whole
test dataset which were promising numbers.

\subsection{Lessons Learned} \label{sec:feas_lessons}

Conducting the reproduction study assured us that clone detection
has a high potential to be solved by a combination of metric
based and machine learning based approaches. Also, it helped us
understand what are the main issues that should be tackled in this
approach. A summary of learned lessons include:
\begin{itemize}
	\item Software metrics have a great potential for being used
          in finding code clone pairs. However, they need to be
          engineered so that they can capture meaningful properties
          that are useful in clone detection. Analyzing false
          negatives in this study revealed that when two features have
          a small value, even with a very small difference, their
          Percentage Difference gets large. Consider 1 and 2 which have 50\% percentage difference; this values equals to percentage difference between 10 and 20, but these two cases should be
          treated differently. Therefore, we need to engineer features
          in a way that differences are captured properly.
	\item Random Forest algorithm has a better performance
          compared to other algorithms for this problem and on our
          metrics.
	\item Analysis of false positives revealed many code fragments
          that are not real clone pairs, but have identical
          structures. Hence, we need additional features that beside
          capturing the structural similarity, can recognize semantic
          similarity.
	\item This approach is susceptible to the problem of
          \textit{Candidate Explosion} as every possible method pair
          needs to be generated. In this study, the problem was
          tackled by limiting the number of clone pairs based on their
          differences in terms of number of statements. However, this
          solution is not possible to be applied on all datasets,
          especially when they get large. Thus, a more robust
          approach is needed in order to be able to scale.
\end{itemize}
